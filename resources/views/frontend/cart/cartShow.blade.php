@extends('frontend.layouts.app')
@section('showcart')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Shopping Cart</li>
            </ol>
        </div>
        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Images</td>
                        <td class="description">Name</td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                  @php
                        if(empty(session()->has('cart'))){
                            print_r('Bạn chưa thêm sản phẩm vào giỏ hàng !!!');
                        }else{
                            $sessionData = session()->get('cart');

                                foreach($sessionData as $key => $value){
                  @endphp
                  <tr>
                        <td class="cart_product">
                            <a href=""><img src="{{ asset('upload/products/'.$value['id'].'/'.json_decode($value['hinhanh'],true)[0])}}" alt="ảnh" style="object-fit: cover; object-position: top;" width="150" height="100"></a>
                        </td>
                        <td class="cart_description" style="padding-top: 25px;">
                            <h4 style="width: 300px;">{{ $value['name']}}</h4>
                        </td>
                        <td class="cart_price" style="padding-top: 35px;">
                            <p>$ {{ceil($value['price'])}}</p>
                        </td>
                        <td class="cart_quantity" style="padding-top: 35px;">
                            <div class="cart_quantity_button">
                                <a id="{{$value['id']}}" class="cart_quantity_up" style="text-decoration: none; cursor: pointer;"> + </a>
                                <input class="cart_quantity_input" type="text" name="quantity" value="{{ceil($value['quantity'])}}" autocomplete="off" min="1" size="2">
                                <a id="{{$value['id']}}" class="cart_quantity_down" style="text-decoration: none; cursor: pointer;"> - </a>
                            </div>
                        </td>
                        <td class="cart_total" style="padding-top: 35px;">
                            <p id="{{$value['id']}}" class="cart_total_price">$ {{ ceil($value['total'])}}</p>
                        </td>
                        <td class="cart_delete" >
                            <a id="{{$value['id']}}" class="cart_quantity_delete"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                  @php
                            }
                        }
                  @endphp
                </tbody>
            </table>
        </div>
    </div>
</section>
<!--/#cart_items-->
<section id="do_action">
    <div class="container">
        <div class="heading">
            <h3>What would you like to do next?</h3>
            <p>Choose if you have a discount code or reward points you want to use or would like to estimate your
                delivery cost.</p>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="chose_area">
                    <ul class="user_option">
                        <li>
                            <input type="checkbox">
                            <label>Use Coupon Code</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Use Gift Voucher</label>
                        </li>
                        <li>
                            <input type="checkbox">
                            <label>Estimate Shipping & Taxes</label>
                        </li>
                    </ul>
                    <ul class="user_info">
                        <li class="single_field">
                            <label>Country:</label>
                            <select>
                                <option>United States</option>
                                <option>Bangladesh</option>
                                <option>UK</option>
                                <option>India</option>
                                <option>Pakistan</option>
                                <option>Ucrane</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>

                        </li>
                        <li class="single_field">
                            <label>Region / State:</label>
                            <select>
                                <option>Select</option>
                                <option>Dhaka</option>
                                <option>London</option>
                                <option>Dillih</option>
                                <option>Lahore</option>
                                <option>Alaska</option>
                                <option>Canada</option>
                                <option>Dubai</option>
                            </select>

                        </li>
                        <li class="single_field zip-field">
                            <label>Zip Code:</label>
                            <input type="text">
                        </li>
                    </ul>
                    <a class="btn btn-default update" href="">Get Quotes</a>
                    <a class="btn btn-default check_out" href="">Continue</a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="total_area">
                    @php
                     $getTotal = 0;
                     $tax = 2;
                        if(empty(session()->has('cart'))){
                            print_r(0);
                        }else{
                            $cartTotal = session()->get('cart');
                            foreach( $cartTotal as $key => $value){
                                $getTotal += $cartTotal[$key]['quantity'] * ceil($cartTotal[$key]['price']);
                                $Tong = $getTotal + $tax;
                            }
                            // print_r(ceil($Tong));
                        }
                    @endphp
                    <ul>
                        <li>Cart Sub Total<span>$<span class="spanTotal">{{ceil($getTotal)}}</span></span></li>
                        <li>Eco Tax <span>$2</span></li>
                        <li>Shipping Cost <span>Free</span></li>
                        <li>Total <span>$<span class="Total">
                        @php
                            if(!empty($Tong)){
                                echo ceil($Tong);
                            }
                        @endphp
                        </span></span></li>
                    </ul>
                    <a class="btn btn-default update" href="">Update</a>
                    <a class="btn btn-default check_out" href="{{route('cart.checkout')}}">Check Out</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#do_action-->
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
            });
            var ecoTax = 2;
        $('a.cart_quantity_up').click(function(){
            var getId = $(this).attr('id');

            var getPrice = $(this).closest("tr").find('.cart_price p').text();
            // cách chuổi ta dùng slipt
            var String__Price = getPrice.split("$");
            for(var i = 0; i< String__Price.length ; i++ ){
                var Price = String__Price[i];
            }
            var getQuantity = $(this).closest('tr').find('.cart_quantity_input').val();
            $(this).closest('tr').find('.cart_quantity_input').val(parseInt(getQuantity)+1);
            var gettotal = $(this).closest('tr').find('.cart_total p').text();
            $(this).closest('tr').find('.cart_total p').text("$ " + parseInt((Price * (parseInt(getQuantity)+1))) );

            //tinh tong
            var subTotal = $(".spanTotal").text();
            var sum = parseInt(Price) +  parseInt(subTotal);
            $(".spanTotal").text(sum);
            var Total = $(".Total").text();
            $(".Total").text(parseInt(sum) + 2);
            // console.log(sum);

                $.ajax({
                    type:'POST',
                    url: '{{route('cart.addTocart')}}',
                    data:{
                        "getID" : getId,
                    }
                }).done(function(result){
                    console.log(result);
                });
        });
        $('.cart_quantity_down').click(function(){
            var getIdDown = $(this).attr('id');
            var getPrice = $(this).closest('tr').find('.cart_price p').text();
            var getName = $(this).closest('tr').find('.cart_description h4').text();
            var getsubTotal = $('.spanTotal').text();
            var getTotal = $('.Total').text();
            //cắt chuỗi ta dùng split()
            for(var i = 0; i < getPrice.split('$').length; i++){
                var price = getPrice.split('$')[i];
            }
            var getQuantity = $(this).closest('tr').find('.cart_quantity_input').val();
                              $(this).closest('tr').find('.cart_quantity_input').val(parseInt(getQuantity) -1);

            var total = $(this).closest('tr').find('.cart_total p ').text();
                        $(this).closest('tr').find('.cart_total p ').text("$ " + (parseInt(price) * (parseInt(getQuantity)-1)));

            if(parseInt(getQuantity) <= 1){
                if(confirm('Bạn có muốn xóa sản phầm ' +  getName + ' này không? ')){
                    $('.spanTotal').text(parseInt(getsubTotal) - parseInt(price));
                    $('.Total').text((parseInt(getsubTotal) - parseInt(price)) + parseInt(ecoTax));
                    $(this).closest('tr').remove();
                }else{
                    var quantity = 1;
                    $(this).closest('tr').find('.cart_quantity_input').val(parseInt(quantity));
                    $(this).closest('tr').find('.cart_total p').text("$ " + (parseInt(price) * parseInt(quantity)));
                    return true;
                }

            }else{
                $('.spanTotal').text(parseInt(getsubTotal) - parseInt(price));
                $('.Total').text((parseInt(getsubTotal) - parseInt(price)) + parseInt(ecoTax));
                //  console.log(total);

            }
            $.ajax({
                type:'POST',
                url : '{{route('cart.downTocart')}}',
                data:{
                    'getDownId': getIdDown,
                }
                }).done(function(result){
                console.log(result);
           });
            // console.log(total);
        });
        $('.cart_quantity_delete').click(function(){
           var getId  = $(this).attr('id');
           $(this).closest('tr').remove();
           var getpricetpTotal = $(this).closest('tr').find('.cart_total_price').text();
            for(var i = 0 ; i< getpricetpTotal.split("$").length; i++){
                var total = getpricetpTotal.split("$")[i];
            }
           var getsubCart = $('.spanTotal').text();
            var sum = parseInt(getsubCart) - parseInt(total);
            $('.spanTotal').text(sum);
            var sumTotal = $('.Total').text();
            $('.Total').text(sum +ecoTax );
            // console.log(sum);
            $.ajax({
                type:"POST",
                url : "{{route('cart.deleteajax')}}",
                data:{
                    'deleteId':getId,
                }
            }).done(function(result){
                console.log(result);
            });
        });
    });
</script>
@endsection
