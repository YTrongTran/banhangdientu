@extends('frontend.layouts.app')
@section('checkout')
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li class="active">Check out</li>
            </ol>
        </div><!--/breadcrums-->

        <div class="step-one">
            <h2 class="heading">Step1</h2>
        </div>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible">
                <button class="close" data-dismiss="alert" aria-hidden="true" type="button">X</button>
                <h4><i class="icon fa fa--check"></i>Thông báo!</h4>
                {{ session('success') }}
            </div>
         @endif
         @if (session('error'))
            <div class="alert alert-danger">
                <button class="close" data-dismiss="alert" aria-hidden="true" type="button">X</button>
                <h4><i class="icon fa fa--check"></i>Thông báo!</h4>
                {{ session('error') }}
            </div>
         @endif
         <div class="checkout-options">
             <h3>New User</h3>
             <p>Checkout options</p>
             <ul class="nav">
                 <li>
                     <label><input type="checkbox" id="checkbox"> Register Account</label>
                    </li>
                </ul>
            @unless (Auth::check())
            <p class="dropdown__text">Bạn chưa đăng ký <strong>VUI LÒNG CHECKBOX "Register Account" ở trên !!!</strong></p>
            <form style="display: none;" action="{{ route('cart.register') }}" class="form-horizontal form-material" method="POST"     enctype="multipart/form-data">
                @csrf
                    <div class="form-group" >
                        <label class="col-md-12">Display Name</label>
                        <div class="col-md-12">
                            <input type="text" placeholder="Display Name" name="name"
                                class="@error('name') is-invalid @enderror"
                                class="form-control form-control-line">
                        </div>
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="example-email" class="col-md-12">Email</label>
                        <div class="col-md-12">
                            <input type="email" name="email" placeholder="shop@gmail.com"
                                class="@error('email') is-invalid @enderror"
                                class="form-control form-control-line" name="email" id="example-email">
                        </div>
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Password</label>
                        <div class="col-md-12">
                            <input type="password" name="password"
                                class="@error('password') is-invalid @enderror"
                                class="form-control form-control-line">
                        </div>
                        @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Phone</label>
                        <div class="col-md-12">
                            <input type="text" placeholder="0905000555" name="phone"
                                class="@error('phone') is-invalid @enderror"
                                class="form-control form-control-line">
                        </div>
                        @error('phone')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <button class="btn btn-success" type="submit">Register User</button>
                        </div>
                    </div>
            </form>



            </div><!--/checkout-options-->
            {{-- login --}}
        <form action="{{ route('cart.sing_up') }}" method="POST">
            @csrf
            <input type="text" placeholder="email" name="email"
                class="@error('email') is-invalid @enderror" />
            @error('email')
                <div class="alert alert-danger" style="display: inline-block;">{{ $message }}</div>
            @enderror

            <input type="password" name="password" placeholder="password"
                class="@error('password') is-invalid @enderror" />

            @error('password')
                <div class="alert alert-danger" style="display: inline-block;">{{ $message }}</div>
            @enderror

            <button type="submit" class="btn btn-default">Login</button>
        </form>
        {{-- login --}}

        @endunless
        <div class="register-req">
            <p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
        </div><!--/register-req-->

        <div class="shopper-informations">
            <div class="row">
                <div class="col-sm-3">
                    <div class="shopper-info">
                        <p>Shopper Information</p>
                        <form action="{{route('checkout')}}" method="POST">
                            @csrf
                                <input type="text" name="name"  placeholder="Display Name" class="@error('name') is-invalid @enderror">
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input type="text" name="email"  placeholder="User Email" class="@error('email') is-invalid @enderror">
                                @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input type="text" name="phone"  placeholder="User Phone" class="@error('phone') is-invalid @enderror">
                                @error('phone')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input type="text" name="address"  placeholder="User Address" class="@error('address') is-invalid @enderror">
                                @error('address')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            <button type="submit" name="submit" class="btn btn-primary">Đặt Hàng</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <div class="review-payment">
            <h2>Review & Payment</h2>
        </div>

        <div class="table-responsive cart_info">
            <table class="table table-condensed">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Item</td>
                        <td class="description"></td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    @php
                        if (!empty(session()->has('cart'))){
                            $getData = session()->get('cart');
                            foreach($getData as $key => $value){
                                @endphp
                                <tr>
                                    <td class="cart_product">
                                        <a href="#"><img src="{{URL::to('upload/products/'.$value['id'].'/'.json_decode($value['hinhanh'],true)[0])}}"  style="object-fit: cover; object-position: top;" width="150" height="100" alt="ảnh item"></a>
                                    </td>
                                    <td class="cart_description" style="padding-top: 25px;">
                                        <h4 style="width: 300px;"><a href="#">{{$value['name']}}</a></h4>
                                    </td>
                                    <td class="cart_price" style="padding-top: 35px;">
                                        <p>${{ceil( $value['price']) }}</p>
                                    </td>
                                    <td class="cart_quantity" style="padding-top: 35px;">
                                        <div class="cart_quantity_button">
                                            <a id="{{ $value['id'] }}" class="cart_quantity_up" style="text-decoration: none; cursor: pointer;"> + </a>
                                            <input class="cart_quantity_input" type="text" name="quantity" value="{{$value['quantity']}}" autocomplete="off" size="2">
                                            <a id="{{ $value['id'] }}" class="cart_quantity_down"  style="text-decoration: none; cursor: pointer;"> - </a>
                                        </div>
                                    </td>
                                    <td class="cart_total" style="padding-top: 35px;">
                                        <p class="cart_total_price">$ {{ ceil($value['total']) }}</p>
                                    </td>
                                    <td class="cart_delete">
                                        <a id="{{ $value['id'] }}" class="cart_quantity_delete"  href=""><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                                @php

                            }
                        }
                    @endphp


                    <tr>
                        <td colspan="4">&nbsp;</td>
                        <td colspan="2">
                            <table class="table table-condensed total-result">
                                @php
                                    $exoTax =2;
                                    $getsubTotal = 0;
                                    if(empty(session()->has('cart'))){
                                        print_r(0);
                                    }else{
                                        $getDataTotal = session()->get('cart');
                                        foreach ($getDataTotal as $key => $value) {
                                            # code...
                                            $getsubTotal += $getDataTotal[$key]['quantity']*ceil($getDataTotal[$key]['price']);
                                            $getTotal = $exoTax +  $getsubTotal;
                                        }
                                    }
                                @endphp
                                <tr>
                                    <td>Cart Sub Total</td>
                                    <td >$ <span class="spanTotal">{{ ceil($getsubTotal)}}</span></td>
                                </tr>
                                <tr>
                                    <td>Exo Tax</td>
                                    <td>$2</td>
                                </tr>
                                <tr class="shipping-cost">
                                    <td>Shipping Cost</td>
                                    <td>Free</td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td><span class="Total">
                                    @php
                                        if(!empty($getTotal )){
                                            echo ceil($getTotal);
                                        }
                                    @endphp
                                    </span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="payment-options">
                <span>
                    <label><input type="checkbox"> Direct Bank Transfer</label>
                </span>
                <span>
                    <label><input type="checkbox"> Check Payment</label>
                </span>
                <span>
                    <label><input type="checkbox"> Paypal</label>
                </span>
            </div>
    </div>
</section>
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
            });

            var ecoTax = 2;
            $('a.cart_quantity_up').click(function(){
            var getId = $(this).attr('id');
            var getPrice = $(this).closest("tr").find('.cart_price p').text();
            var String__Price = getPrice.split("$");
            for(var i = 0; i< String__Price.length ; i++ ){
                var Price = String__Price[i];
            }
            var getQuantity = $(this).closest('tr').find('.cart_quantity_input').val();
            $(this).closest('tr').find('.cart_quantity_input').val(parseInt(getQuantity)+1);
            var gettotal = $(this).closest('tr').find('.cart_total p').text();
            $(this).closest('tr').find('.cart_total p').text("$ " + parseInt((Price * (parseInt(getQuantity)+1))) );

            //tinh tong
            var subTotal = $(".spanTotal").text();
            var sum = parseInt(Price) +  parseInt(subTotal);
            $(".spanTotal").text(sum);
            var Total = $(".Total").text();
            $(".Total").text(parseInt(sum) + 2);
            // console.log(sum);

                $.ajax({
                    type:'POST',
                    url: '{{route('cart.addTocart')}}',
                    data:{
                        "getID" : getId,
                    }
                }).done(function(result){
                    console.log(result);
                });
            });


        $('.cart_quantity_down').click(function(){
            var getIdDown = $(this).attr('id');
            var getPrice = $(this).closest('tr').find('.cart_price p').text();
            var getName = $(this).closest('tr').find('.cart_description h4').text();
            var getsubTotal = $('.spanTotal').text();
            var getTotal = $('.Total').text();
            //cắt chuỗi ta dùng split()
            for(var i = 0; i < getPrice.split('$').length; i++){
                var price = getPrice.split('$')[i];
            }
            var getQuantity = $(this).closest('tr').find('.cart_quantity_input').val();
                              $(this).closest('tr').find('.cart_quantity_input').val(parseInt(getQuantity) -1);

            var total = $(this).closest('tr').find('.cart_total p ').text();
                        $(this).closest('tr').find('.cart_total p ').text("$ " + (parseInt(price) * (parseInt(getQuantity)-1)));

            if(parseInt(getQuantity) <= 1){
                if(confirm('Bạn có muốn xóa sản phầm ' +  getName + ' này không? ')){
                    $('.spanTotal').text(parseInt(getsubTotal) - parseInt(price));
                    $('.Total').text((parseInt(getsubTotal) - parseInt(price)) + parseInt(ecoTax));
                    $(this).closest('tr').remove();
                }else{
                    var quantity = 1;
                    $(this).closest('tr').find('.cart_quantity_input').val(parseInt(quantity));
                    $(this).closest('tr').find('.cart_total p').text("$ " + (parseInt(price) * parseInt(quantity)));
                    return true;
                }

            }else{
                $('.spanTotal').text(parseInt(getsubTotal) - parseInt(price));
                $('.Total').text((parseInt(getsubTotal) - parseInt(price)) + parseInt(ecoTax));
                //  console.log(total);

            }
            $.ajax({
                type:'POST',
                url : '{{route('cart.downTocart')}}',
                data:{
                    'getDownId': getIdDown,
                }
                }).done(function(result){
                console.log(result);
           });
            // console.log(total);
        });

        $('.cart_quantity_delete').click(function(){
           var getId  = $(this).attr('id');
           $(this).closest('tr').remove();
           var getpricetpTotal = $(this).closest('tr').find('.cart_total_price').text();
            for(var i = 0 ; i< getpricetpTotal.split("$").length; i++){
                var total = getpricetpTotal.split("$")[i];
            }
           var getsubCart = $('.spanTotal').text();
            var sum = parseInt(getsubCart) - parseInt(total);
            $('.spanTotal').text(sum);
            var sumTotal = $('.Total').text();
            $('.Total').text(sum +ecoTax );
            // console.log(sum);
            $.ajax({
                type:"POST",
                url : "{{route('cart.deleteajax')}}",
                data:{
                    'deleteId':getId,
                }
            }).done(function(result){
                console.log(result);
            });
        });


        $("#checkbox").click(function(){
            var check = $(this).closest('.checkout-options').find("form").hasClass('registerUser');
            if(check == true){
                $(this).closest('.checkout-options').find('form').removeClass('registerUser');
                $(this).closest('.checkout-options').find('form').hide('registerUser');
            }else{
                $(this).closest('.checkout-options').find('form').addClass('registerUser');
                $(this).closest('.checkout-options').find('form').show('registerUser');
                $(this).closest('.checkout-options').find('p.dropdown__text').hide();
            }
            // alert(check);

        });
    });
</script>
@endsection

