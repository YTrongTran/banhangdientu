<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div style="text-align: center; margin: 0 auto; padding: 30px; background-color: #eee;">
        <h2>Hi {{$order_email}}</h2>
        <p><strong style="background-color: orange;">Bạn đã đặt đơn hàng thành công tại cửa hàng chúng tôi</strong></p>
        <h3>Thông tin đơn hàng của bạn {{$order_name}} </h3>
        <h4>Ngày đặt đơn hàng {{ $now }}</h4>


        <h4>Chi tiết đơn hàng</h4>


        <table style=" width: 100%;
                font-family: arial, sans-serif;
                border-collapse: collapse; border-radius: 4px;">
            <thead style="padding: 8px;
                    text-align: left;
                    border-top: 1px solid #dee2e6;">
                <tr style="  padding: 8px;
                text-align: left;
                border-top: 1px solid #dee2e6; background-color: rgba(189, 250, 77, 0.5);">
                    <td >ID</td>
                    <td >Name</td>
                    <td >Price</td>
                    <td >Quantity</td>
                    <td >ToTal</td>
                </tr>

            </thead>
            <tbody style="padding: 8px;text-align: left; background-color: rgba(189, 250, 77, 0.5);">
                <?php $i = 1; ?>
                    @foreach ($getSession as $item)
                        <tr >
                            <td>{{ $i }}</td>
                            <td style="width: 300px;">{{ $item['name']}}</td>
                            <td>$ {{ ceil($item['price'])}}</td>
                            <td>{{ $item['quantity']}}</td>
                            <td>$ {{ceil($item['price'] * $item['quantity']) }}</td>
                        </tr>
                        <?php $i++; ?>
                    @endforeach

            </tbody>

        </table>
        <table style="margin-left:auto;" >
            <thead style=" background-color: rgba(255, 166, 0, 0.555);">
                <tr>
                    <td scope="row" colspan="2">Cart Sub Total </td>
                    <td scope="row" colspan="2">Exo Tax  </td>
                    <td scope="row" colspan="2">Shipping Cost </td>
                    <td scope="row" colspan="2">Total </td>
                </tr>
            </thead>
            <tbody style=" background-color: rgba(255, 166, 0, 0.555);" >
                <tr>
                  <td colspan="2"> $ {{ ceil($sum) }} </td>
                  <td colspan="2"> $ 2</td>
                  <td colspan="2"> Free</td>
                  <td colspan="2"> $ {{ ceil($sum) + 2}} </td>
                </tr>
              </tbody>
        </table>

    </div>
</body>
</html>
