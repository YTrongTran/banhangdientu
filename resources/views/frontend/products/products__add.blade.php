@extends('frontend.member.account')
@section('sm-col')
    <div class="col-sm-9">
        <div class="signup-form">
           
            <h2>Create Products!</h2>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif

            <form action="{{ route('products.add') }}" enctype="multipart/form-data" method="POST">
                @csrf

                <div class="Name">
                    <label>Name</label>
                    <input id="signup_name" class="@error('name') is-invalid @enderror" value="{{ old('name') }}"
                        type="text" placeholder="Name" name="name" />
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="price">
                    <label>Price</label>
                    <input id="signup_email" type="text" value="{{ old('price') }}" placeholder="Price" name="price"
                        class="@error('price') is-invalid @enderror" />
                    @error('price')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="Id_Categogry">
                    <label>Id_Categogry</label>
                    <select name="id_category" class="@error('id_category') is-invalid @enderror">
                        <option value="">
                            ----Vui
                            lòng chọn -----
                        </option>
                        @foreach ($id_category as $key => $value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('id_category')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>
                <div class="Id_Brand">
                    <label>Id_Brand</label>
                    <select name="id_brand" class="@error('id_brand') is-invalid @enderror">
                        <option value="" class="@error('id_brand') is-invalid @enderror">----Vui lòng
                            chọn -----
                        </option>
                        @foreach ($id_brand as $key => $value)
                            <option value="{{ $key }}">{{ $value }}
                            </option>
                        @endforeach
                    </select>
                    @error('id_brand')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="Status">
                    <label>Status</label>
                    <select id="status" name="status" class="@error('status') is-invalid @enderror">
                        <option value="" class="@error('status') is-invalid @enderror">----Vui lòng chọn -----</option>
                        <option value="0">New</option>
                        <option value="1">Sale</option>
                    </select>
                    @error('status')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="Sale">
                    <label>Sale</label>
                    <input class="signup_sale" id="signup_sale"  type="text" name="sale" value="0"
                        style="width: 30%; position: absolute; display: none;" />
                    <span style="display: none;position: relative; margin-left: 238px; top: 35px; font-weight: bold"
                      id="signup_span"  class="@error('sale') is-invalid @enderror">%</span>

                    @error('sale')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="Company" style="margin-top: 51px;">
                    <label>Company</label>
                    <input id="signup_name"  style="margin-top: 37px"
                        class="@error('company') is-invalid @enderror" type="text" placeholder="Company profile"
                        name="company" value="" />
                    @error('company')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div>
                    <label>Avatar</label>
                        <input type="file" value="" placeholder="hinh anh" id="hinhanh" name="hinhanh[]" multiple />
                    @error('hinhanh')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Detail</label>
                    <div class="form-group">
                        <textarea class="form-control" name="detail" class="@error('detail') is-invalid @enderror"
                            rows="5"></textarea>
                    </div>
                    @error('detail')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button style="margin-top: 37px" type="submit" class="btn btn-default">Signup</button>
            </form>
        </div>
        <script>
            var limit = 3;
            $(document).ready(function() {
                // $('#hinhanh').click(function() {
                //     alert('oke');
                // });

                // $('#hinhanh').change(function() {
                //     var files = $(this)[0].files;
                //     // var getImg = $(this).val();
                //     // console.log(getImg);

                //     if (files.length > limit) {
                //         alert("You can select max " + limit + " images.");
                //         $('#hinhanh').val('');
                //         return false;
                //     } else {
                //         return true;
                //     }
                // });
                // $('#signup_sale').load(){
                //     $(this).hide();
                // }
                // $("#signup_sale").mouseenter(function(){
                //      $(this).hide();
                // });
                $('#status').click(function(){
                    var status = $(this).val();

                    if(status == ""){
                        $(this).closest('form').find('.signup_sale').hide();
                        $(this).closest('form').find('#signup_span').hide();
                        
                    }else{
                        $(this).closest('form').find('.signup_sale').show();
                        $(this).closest('form').find('#signup_span').show();

                    }

                });
            });
        </script>
    </div>

@endsection
