@extends('frontend.member.account')
@section('sm-col')
    <div class="col-sm-9">
        <div class="signup-form">
            <h2>Edit Products!</h2>
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
             @endif
            <form action="{{ route('products.edit', ['id' => $idProducts['id']]) }}" method="POST"
                enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{ $idProducts['id'] }}">
                <div class="Name">
                    <label>Name</label>
                    <input id="signup_name" class="@error('name') is-invalid @enderror" value=" {{ $idProducts['name'] }}"
                        type="text" placeholder="Name" name="name" />

                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="price">
                    <label>Price</label>
                    <input id="signup_email" type="text" value="{{ $idProducts['price'] }}" placeholder="Price"
                        name="price" class="@error('price') is-invalid @enderror" />
                    @error('price')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="Id_Categogry">

                    <label>Id_Categogry</label>
                    <select name="id_category" class="@error('id_category') is-invalid @enderror">
                        <option value="{{ old('id_category') }}" class="@error('id_category') is-invalid @enderror">
                            ----Vui
                            lòng chọn -----
                        </option>
                        @foreach ($id_category as $key => $value)
                            <option value="{{ $key }}" {{ $key == $idProducts->id_category ? 'selected' : ' ' }}>
                                {{ $value }}</option>
                        @endforeach
                    </select>
                    @error('id_category')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>
                <div class="Id_Brand">

                    <label>Id_Brand</label>
                    <select name="id_brand" class="@error('id_brand') is-invalid @enderror">
                        <option value="{{ old('id_brand') }}" class="@error('id_brand') is-invalid @enderror">----Vui
                            lòng
                            chọn -----
                        </option>
                        @foreach ($id_brand as $key => $value)
                            <option value="{{ $key }}" {{ $key == $idProducts->id_brand ? 'selected' : ' ' }}>
                                {{ $value }}
                            </option>
                        @endforeach
                    </select>
                    @error('id_brand')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="Status">
                    <label>Status</label>
                    <select id="status" name="status" class="@error('status') is-invalid @enderror">
                        <option value="" class="@error('status') is-invalid @enderror">----Vui lòng chọn -----</option>
                        <option value="0" {{ $idProducts->status === 0 ? 'selected' : ' ' }}>New</option>
                        <option value="1" {{ $idProducts->status === 1 ? 'selected' : ' ' }}>Sale</option>
                    </select>
                    @error('status')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="Sale">
                    <label>Sale</label>
                    <input class="signup_sale" value="{{ $idProducts['sale'] }}" type="text" name="sale" value="0"
                        style="width: 30%; position: absolute;" />
                    <span id="signup_span" style="position: relative; margin-left: 238px; top: 35px; font-weight: bold"
                        class="@error('sale') is-invalid @enderror">%</span>

                    @error('sale')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="Company" style="margin-top: 51px;">
                    <label>Company</label>
                    <input id="signup_name" value="{{ $idProducts['company'] }}" style="margin-top: 37px"
                        class="@error('company') is-invalid @enderror" type="text" placeholder="Company profile"
                        name="company" value="" />
                    @error('company')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="Avatar">

                    <label>Avatar</label>
                    <input value="{{ $idProducts['hinhanh'] }}" type="file" placeholder="hinh anh" name="hinhanh[]"
                        multiple />
                    @foreach ($getImg as $item)
                        <img class="images"
                            style="position: relative; ;border: 2px solid black; object-fit: cover; margin-right: 20px;margin-left: 20px;display: inline-block;"
                            src="{{ asset('upload/products/' . $idProducts->id . '/' . $item) }}" width="100px" height="100px"
                            alt="">

                        <input type="checkbox" name="array[]" value="{{ $item }}" style="position: absolute; width: 90px;
                         bottom: 220px; height: 50px;margin-top: 5px;margin-left: -120px;display: inline-block;">
                    @endforeach

                    @error('hinhanh')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group" style="margin-top: 40px;">
                    <label>Detail</label>
                    <div class="form-group">
                        <textarea class="form-control" name="detail" class="@error('detail') is-invalid @enderror"
                            rows="5">{{ $idProducts['detail'] }}</textarea>
                    </div>
                    @error('detail')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <button style="margin-top: 37px" type="submit" class="btn btn-default">Signup</button>
            </form>
        </div>

        <script>
            $(document).ready(function() {
                // $('#hinhanh').click(function() {
                //     alert('oke');
                // });

                // $('#hinhanh').change(function() {
                //     var files = $(this)[0].files;
                //     if (files.length > limit) {
                //         alert("You can select max " + limit + " images.");
                //         $('#hinhanh').val('');
                //         return false;
                //     } else {
                //         return true;
                //     }
                // });
                //remove
                // $('input.checkbox').click(function() {
                //         //kiểm tra cái class đó

                //         var check = $('img').hasClass('images');
                //         if (check == true) {
                //             $('img').hide().removeClass('images');
                //             $(this).hide();
                //             }
                //             // else{
                //             //     $('img').show().addClass('images');
                //             // }
                //         });
                // });
                $('#status').click(function() {
                    var status = $(this).val();

                    if (status == "") {
                        $(this).closest('form').find('.signup_sale').hide();
                        $(this).closest('form').find('#signup_span').hide();

                    } else {
                        $(this).closest('form').find('.signup_sale').show();
                        $(this).closest('form').find('#signup_span').show();

                    }

                });
            });
        </script>
    </div>

@endsection
