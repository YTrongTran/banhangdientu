@extends('frontend.member.account')
@section('sm-col')
    <div class="col-sm-9">
        <section id="cart_items">
            <div class="container">
                <div class="table-responsive cart_info" style="width: 74%!important;">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td>ID</td>
                                <td>Name</td>
                                <td>Image</td>
                                <td>Price</td>
                                <td>Sale</td>
                                <td>ToTal</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                                <?php $i = 1; ?>
                                @foreach ($productsList as $item)
                                    <tr>
                                        <td scope="row">{{ $i++ }}</td>
                                        <td style="width: 300px;">{{ $item->name }}</td>
                                        <td>
                                            <img style="object-fit: cover;background-position: center" src="{{asset('upload/products/'.$item->id.'/'.json_decode($item->hinhanh,true)[0])}}" width="90" height="90" alt="products">
                                        </td>
                                        <td>$ {{ $item->price }}</td>
                                        <td>{{ $item->sale }} %</td>
                                        <td>$ {{ ($item->price * (100 - $item->sale)) / 100 }}</td>
                                        <td>
                                            <a href="{{ route('products.edit', ['id' => $item->id]) }}"
                                                style="margin-right: 20px;">
                                                Edit
                                                <i class="mdi mdi-account-edit"></i>
                                            </a>
                                            <a href="{{ route('products.delete', ['id' => $item->id]) }}"
                                                style="color: red; margin-right: 20px;">Delete
                                                <i class="mdi mdi-delete-sweep"></i>
                                            </a>

                                        </td>
                                    </tr>
                                @endforeach

                        </tbody>
                    </table>
                    <div style="display: inline;text-align:center">{{ $productsList->links() }}</div>
                </div>
            </div>
        </section>
    </div>


@endsection
