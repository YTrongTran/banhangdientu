    <header id="header">
		<div class="header_top">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> info@domain.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="header-middle">
			<div class="container">
				<div class="row">
					<div class="col-md-4 clearfix">
						<div class="logo pull-left">
							<a href="index.html"><img src="{{asset('frontend/images/home/logo.png')}}" alt="logo" /></a>
						</div>
						<div class="btn-group pull-right clearfix">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									USA
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="">Canada</a></li>
									<li><a href="">UK</a></li>
								</ul>
							</div>

							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									DOLLAR
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="">Canadian Dollar</a></li>
									<li><a href="">Pound</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-8 clearfix">
						<div class="shop-menu clearfix pull-right">
							<ul class="nav navbar-nav">
                                @if (empty(Auth::id()))
								<li><a href="{{ route('account.index') }}" ><i class="fa fa-user"></i> Account</a></li>
                                @else
								<li><a href="{{ route('account.edit',['id'=> Auth::id()])}}" ><i class="fa fa-user"></i> Account</a></li>

                                @endif
								<li><a href=""><i class="fa fa-star"></i> Wishlist</a></li>
								<li><a href="{{route('cart.checkout')}}"><i class="fa fa-crosshairs"></i> Checkout</a></li>

								<li><a href="{{route('cart.show')}}"><i class="fa fa-shopping-cart"></i> Cart <span class="cart" >
                               </span>
                                </a></li>

							@if (!empty(Auth::user()->id))
                                <li>
                                    <a class="dropdown-item" href="{{ route('member.logout') }}"
                                         onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                        <i class="ti-email m-r-5 m-l-5"></i>{{ Auth::user()->email }} {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('member.logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>
                            @else
                                <li>
                                    <a class="js-login" href="{{ route('login.dangnhap') }}"><i class="fa fa-lock"></i> Login
                                    </a>
                                </li>
                            @endif
                                {{-- <li><a href="{{ route('member.register') }}"><i class="fa fa-lock"></i> Register</a></li> --}}
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="header-bottom">
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="" class="active">Home</a></li>
								<li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="{{route('homeproducts.index')}}">Products</a></li>


										<li><a href="">Product Details</a></li>

										<li><a href="checkout.html">Checkout</a></li>
										<li><a href="cart.html">Cart</a></li>
										<li><a href="login.html">Login</a></li>
                                    </ul>
                                </li>
								<li class="dropdown"><a href="#">Blog<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="{{ route('fdblog.index') }}">Blog List</a></li>
                                        <li><a href="{{ URL::to('blog/blog-detail/'.'1') }}">Blog Single</a></li>
                                    </ul>
                                </li>
								<li><a href="404.html">404</a></li>
								<li><a href="contact-us.html">Contact</a></li>
							</ul>
						</div>
					</div>
					
					<div class="col-sm-3 ">
						<div class="search_box pull-right">
							<input type="text" id="searchProducts" placeholder="Search"/>
							<div class="search-ajax-result"></div>
						</div>
						@csrf
					</div>
				</div>
			</div>
		</div>
	</header>
	<script>
		$(document).ready(function(){
			$.ajaxSetup({
				    headers: {
				        		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				    		}
					});
		$('.search-ajax-result').hide();
			$('#searchProducts').keyup(function(){
				var query = $(this).val();
				if(query != ""){
					$.ajax({
						type:"GET",
						url: "{{route('searchProducts.textsearch')}}?query="+query,
						success:function(result){
							$('.search-ajax-result').show(400);
							$('.search-ajax-result').html(result);
							 // console.table(result);
						},
					});
				}else{
						$('.search-ajax-result').hide();
						$('.search-ajax-result').html('');
				}
				
			});

			
		});
	</script>


