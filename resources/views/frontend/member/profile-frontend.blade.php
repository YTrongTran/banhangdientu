    @extends('frontend.layouts.app')
    @section('login')
        <section id="form">
            <!--form-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-1">
                        <div class="login-form">
                            <!--login form-->
                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible">
                                    <button class="close" data-dismiss="alert" aria-hidden="true" type="button">X</button>
                                    <h4><i class="icon fa fa--check"></i>Thông báo!</h4>
                                    {{ session('success') }}
                                </div>
                            @endif
                            <h2>Login to your account</h2>

                            <form action="{{ route('login.dangnhap') }}" method="POST">
                                @csrf

                                {{-- <input type="hidden" name="level" /> --}}
                                <input type="text" placeholder="email" name="email"
                                    class="@error('email') is-invalid @enderror" />
                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <input type="password" name="password" placeholder="password"
                                    class="@error('password') is-invalid @enderror" />

                                @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                <span>
                                    <input type="checkbox" name="remember_me" id="remember_me" class="checkbox">
                                    Keep me signed in
                                </span>
                                <button type="submit" class="btn btn-default">Login</button>
                            </form>
                        </div>
                        <!--/login form-->
                    </div>
                    <div class="col-sm-1">
                        <h2 class="or">OR</h2>

                    </div>
                    <div class="col-sm-4">
                        <div class="signup-form">
                            <!--sign up form-->

                            <h2>New User Signup!</h2>
                            <form action="{{ route('member.register') }}" class="form-horizontal form-material"
                                method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="col-md-12">Full Name</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Johnathan Doe" name="name"
                                            class="@error('name') is-invalid @enderror"
                                            class="form-control form-control-line">
                                    </div>
                                    @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <input type="email" placeholder="johnathan@admin.com"
                                            class="@error('email') is-invalid @enderror"
                                            class="form-control form-control-line" name="email" id="example-email">
                                    </div>
                                    @error('email')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Password</label>
                                    <div class="col-md-12">
                                        <input type="password" name="password"
                                            class="@error('password') is-invalid @enderror"
                                            class="form-control form-control-line">
                                    </div>
                                    @error('password')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Phone No</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="123 456 7890" value="" name="phone"
                                            class="@error('phone') is-invalid @enderror"
                                            class="form-control form-control-line">
                                    </div>
                                    @error('phone')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Address</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Da nang City" value="" name="address"
                                            class="@error('address') is-invalid @enderror"
                                            class="form-control form-control-line">
                                    </div>
                                    @error('address')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Avatar</label>
                                    <div class="col-md-12">
                                        <input type="file" name="avatar" class="@error('avatar') is-invalid @enderror"
                                            class="form-control form-control-line">
                                    </div>
                                    @error('avatar')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-12">Country</label>
                                    <div class="col-sm-12">
                                        <select class="form-control form-control-line"
                                            class="@error('id_country') is-invalid @enderror" name="id_country">
                                            <option value="{{old('id_country')}}">---chọn---</option>
                                            @foreach ($country as $key => $value)
                                                <option value="{{ $key }}">{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @error('id_country')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Register Profile</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--/sign up form-->
                    </div>
                </div>
            </div>
        </section>
        <!--/form-->
    @endsection
