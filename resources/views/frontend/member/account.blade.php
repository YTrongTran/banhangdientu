@extends('frontend.layouts.app')
@section('account')

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">

                        <h2>ACCOUNT</h2>

                        <div class="panel-group category-products" id="accordian">
                            <!--category-productsr-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                            ACCOUNT
                                        </a>
                                    </h4>
                                </div>
                                <div id="sportswear" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            @if (empty(Auth::id()))
                                            {{  "Không có dữ liệu" }}
                                            @else
                                            <li><a href="{{ route('account.edit',['id'=> Auth::id()])}}">My Profile </a></li>
                                            @endif

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordian" href="#mens">
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                            ADD PRODUCT
                                        </a>
                                    </h4>
                                </div>
                                <div id="mens" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            @if (empty(Auth::id()))
                                                {{  "Không có dữ liệu" }}
                                            @else
                                            <li><a href="{{route('products.add')}}">ADD PRODUCTS</a></li>
                                            <li><a href="{{route('products.index')}}">MY PRODUCTS</a></li>
                                           <!--  {{-- <li><a href="{{route('products.edit')}}">EDIT PRODUCTS</a></li> --}} -->
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/category-products-->
                    </div>
                </div>
                {{-- <!-- <div class="col-sm-3">
                    @if (session('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button class="close" data-dismiss="alert" aria-hidden="true" type="button">X</button>
                            <h4><i class="icon fa fa--check"></i>Thông báo!</h4>
                            {{ session('success') }}
                        </div>
                    @endif
                </div> --> --}}

                @yield('sm-col')

            </div>
        </div>
    </section>

@endsection
