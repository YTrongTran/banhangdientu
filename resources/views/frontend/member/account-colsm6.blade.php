@extends('frontend.member.account')
@section('sm-col')
    <div class="col-sm-9">
        <div class="signup-form">

            <h2>User Update!</h2>
            @if (session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif

            <form action="{{ route('account.edit', ['id' => Auth::user()->id]) }}" method="post"
                enctype="multipart/form-data">
                @csrf

                <div class="form-controll">
                    <input type="text" class="@error('name') is-invalid @enderror" placeholder="Name" name="name"
                        value="{{ Auth::user()->name }}" />
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="form-controll">
                    <input type="email" disabled placeholder="Email" name="email" value="{{ Auth::user()->email }}" />
                </div>


                <div class="form-controll">
                    <input type="password" placeholder="Password" name="password" />
                </div>

                <div class="form-controll">
                    <input type="text" class="@error('address') is-invalid @enderror" placeholder="Da Nang City"
                        name="address" value="{{ Auth::user()->address }}" />
                    @error('address')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="form-controll">
                    <input type="text" class="@error('phone') is-invalid @enderror" placeholder="Phone" name="phone"
                        value="{{ Auth::user()->phone }}" />
                    @error('phone')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="form-cotroll">
                    <select name="id_country" class="@error('id_country') is-invalid @enderror">
                        <option value="">----Vui lòng chọn -----</option>
                        @foreach ($country as $key => $value)
                            <option value="{{ $key }}" {{ $key == Auth::user()->id_country? 'selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                    @error('id_country')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>

                <div class="form-controll">
                    <input class="@error('avatar') is-invalid @enderror" type="file" placeholder="Avatar" name="avatar"
                        value="{{ Auth::user()->avatar }}" />

                    <img style="object-fit: cover"
                        src="{{ asset('upload/User/avatar/' . $member->id . '/' . Auth::user()->avatar) }}" width="150"
                        height="100" alt="">

                    @error('avatar')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                </div>
                <button type="submit" class="btn btn-default">Signup</button>

            </form>

        </div>
    </div>
@endsection
