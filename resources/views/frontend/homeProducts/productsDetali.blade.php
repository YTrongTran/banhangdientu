@extends('frontend.layouts.app')
@section('menu-left')
    @include('frontend.layouts.menu-left')
@endsection
@section('content')
    <div class="product-details">
        <!--product-details-->

            <div class="col-sm-5">

            <ul id="imageGallery" class="sile">
                @php
                if(!empty(json_decode($details['hinhanh'],true)[0] )){
                    @endphp
                    <li data-thumb="{{asset('upload/products/'.$details['id'].'/'. json_decode($details['hinhanh'],true)[0] )}}" data-src="{{asset('upload/products/'.$details['id'].'/'. json_decode($details['hinhanh'],true)[0] )}}">
                    <img width="100%" height="100%" style="object-fit: cover; " src="{{asset('upload/products/'.$details['id'].'/'. json_decode($details['hinhanh'],true)[0] )}}" />
                     </li>
                    @php
                }
                @endphp
                 @php
                 if(!empty(json_decode($details['hinhanh'],true)[1] )){
                     @endphp
                     <li data-thumb="{{asset('upload/products/'.$details['id'].'/'. json_decode($details['hinhanh'],true)[1] )}}" data-src="{{asset('upload/products/'.$details['id'].'/'. json_decode($details['hinhanh'],true)[1] )}}">
                    <img width="100%" height="100%" style="object-fit: cover; " src="{{asset('upload/products/'.$details['id'].'/'. json_decode($details['hinhanh'],true)[1] )}}" />
                </li>
                     @php
                 }
                 @endphp
                  @php
                  if(!empty(json_decode($details['hinhanh'],true)[2] )){
                      @endphp

                      <li data-thumb="{{asset('upload/products/'.$details['id'].'/'. json_decode($details['hinhanh'],true)[2] )}} "   data-src="{{asset('upload/products/'.$details['id'].'/'. json_decode($details['hinhanh'],true)[2] )}}">
                         <img width="100%" height="100%" style="object-fit: cover; " src="{{asset('upload/products/'.$details['id'].'/'. json_decode($details['hinhanh'],true)[2] )}}" />
                        </li>
                      @php
                  }
                  @endphp
            </ul>

        </div>
        <div class="col-sm-7">
            <div class="product-information">
                @if($details['status'] == 0)
                <img src="{{asset('frontend/images/product-details/new.jpg')}}" class="newarrival" style="object-fit: cover;" alt="new" />
                @else
                <img src="{{asset('frontend/images/product-details/sale.jpg')}}" class="newarrival" style="object-fit: cover;" alt="sale" />
                @endif
                <h2>{{ $details['name'] }}</h2>
              
                <img src="{{ asset('frontend/images/product-details/rating.png')}}"  alt="rating" />


                <span>
                    <span class="price">US $ {{ceil($details['price'] * (100 - $details['sale']) / 100) }}</span>
                    <label>Quantity:</label>

                    <input type="number" min="1" value="1" />
                    <button type="button" class="btn btn-fefault cartShopping" style="background-color: #FE980F">
                        <i id="{{ $details['id']}}" class="fa fa-shopping-cart "></i>
                        Add to cart
                    </button>
                </span>
                <p><b>Availability:</b> In Stock</p>
                <p><b>Condition:</b>
                    @if($details['status'] == 0)
                    New
                    @else
                    Sale
                    @endif
                </p>
                <p><b>Brand:</b> {{ $brand[$details->id_brand] }} </p>
                <a href=""><img src="frontend/images/product-details/share.png" class="share img-responsive" alt="" /></a>
            </div>
        </div>

    </div>
    <!--/product-details-->

    <div class="category-tab shop-details-tab">
        <!--category-tab-->
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li><a href="#details" data-toggle="tab">Details</a></li>
                <li><a href="#companyprofile" data-toggle="tab">Company Profile</a></li>
                <li><a href="#tag" data-toggle="tab">Tag</a></li>
                <li class="active"><a href="#reviews" data-toggle="tab">Reviews (5)</a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane fade" id="details">
                <p>{{ $details['detail']}}</p>
            </div>

            <div class="tab-pane fade" id="companyprofile">    
                <p>{{$details['company']}}</p>
            </div>

            <div class="tab-pane fade" id="tag"></div>

            <div class="tab-pane fade active in" id="reviews">
                <div class="col-sm-12">
                    <ul>
                        <li><a href=""><i class="fa fa-user"></i>EUGEN</a></li>
                        <li><a href=""><i class="fa fa-clock-o"></i>12:41 PM</a></li>
                        <li><a href=""><i class="fa fa-calendar-o"></i>31 DEC 2014</a></li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                        nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate
                        velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    <p><b>Write Your Review</b></p>

                    <form action="#">
                        <span>
                            <input type="text" placeholder="Your Name" />
                            <input type="email" placeholder="Email Address" />
                        </span>
                        <textarea name=""></textarea>
                        <b>Rating: </b> <img src="images/product-details/rating.png" alt="" />
                        <button type="button" class="btn btn-default pull-right">
                            Submit
                        </button>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <!--/category-tab-->
   
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                    headers: {
                             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                    });

            $('#imageGallery').lightSlider({
                gallery: true,
                item: 1,
                loop: true,
                thumbItem: 3,
                slideMargin: 0,
                enableDrag: false,
                currentPagerPosition: 'left',
                onSliderLoad: function(el) {
                    el.lightGallery({
                        selector: '#imageGallery .lslide'
                    });
                }
            });
            $(".cartShopping").click(function(){
                var getId = $(this).closest('.product-details').find('i').attr('id');
                var getprice = $(this).closest('.product-details').find('.price').text();
                for(var i = 0; i< getprice.split('US $').length; i++){
                    var price = getprice.split('US $')[i];
                }

                $.ajax({
                    type:'POST',
                    url : '{{ route('ProductAjax.getProductsAjax')}}',
                    data:{
                        'productsId':getId,
                    }
                }).done(function(result){
                     $('.cart').text(result);
                });
            });


        });
    </script>
@endsection
