@extends('frontend.layouts.app')
@section('menu-left')
    @include('frontend.layouts.menu-left')
@endsection
@section('content')
    <div class="features_items">
        <!--features_items-->
        <h2 class="title text-center">Products Items</h2>
        @if( Session::has("success") )
            <div class="alert alert-success alert-block" role="alert">
                <button class="close" data-dismiss="alert"></button>
                {{ Session::get("success") }}
            </div>
        @endif
        @foreach ($products as $item)
            <div class="col-sm-4">
                <div class="product-image-wrapper">
                    <div class="single-products">
                        <div class="productinfo text-center">
                            {{-- mã hóa ảnh tại đây, để mã hóa ảnh tại đây ta dùng foreach 1 lần nữa --}}
                            {{-- {{ $item['hinhanh']}} --}}

                            {{-- {{ var_dump(json_decode($item->hinhanh,true)[0] )}} --}}

                            {{-- {{ dd($value)}} --}}
                            @php
                                if(!empty(json_decode($item->hinhanh,true)[0])){
                            @endphp
                            <img src="{{ asset('upload/products/' . $item->id . '/' .json_decode($item->hinhanh,true)[0]) }}"   style="object-fit: cover; object-position: top; " alt="products" />
                            @php
                                }
                            @endphp

                            <h2>${{ ceil(($item->price * (100 - $item->sale)) / 100) }}</h2>
                            <p style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;">{{ $item->name }}</p>
                            <a type="value" href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to
                                cart</a>
                        </div>
                        <div class="product-overlay">
                            <div class="overlay-content">
                                <h2>$ {{ ceil(($item->price * (100 - $item->sale)) / 100) }} </h2>
                                 <p style="white-space: nowrap; overflow: hidden; text-overflow:ellipsis;" > {{ $item->name }} </p>
                                <a  id="{{ $item->id }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to
                                    cart</a>
                            </div>
                        </div>
                        @if ($item->status == 0)
                            <img src="{{ asset('/frontend/images/home/new.png') }}" class="new" />
                        @else
                            <img src="{{ asset('/frontend/images/home/sale.png') }}" class="new" />
                        @endif

                    </div>
                    <div class="choose">
                        <ul class="nav nav-pills nav-justified">

                            <li><a href="{{ route('details.show', ['id' => $item->id]) }}"><i class="fa fa-plus-square"></i>Product-details</a></li>


                            <li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        @endforeach
        <ul class="pagination">
            <li class="active">{{ $products->links() }}</a></li>
        </ul>
    </div>
    <!--features_items-->
        <script>

            $(document).ready(function(){
                $.ajaxSetup({
                    headers: {
                             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                    });
                $(".add-to-cart").click(function(){
                    var url = "{{ route('ProductAjax.getProductsAjax') }}";
                    var getId = $(this).attr('id');
                    var getImg = $(this).closest('.single-products').find('.productinfo img').attr('src');
                    var getName = $(this).closest('.single-products').find('.overlay-content p').text();
                    var getPrice = $(this).closest('.single-products').find('.overlay-content h2').text();
                    var price = getPrice.split('$');
                    for(var i=0; i < price.length; i++ ){
                        var aPrice = price[i];
                    }
                   console.log(getId);
                   console.log(getImg);
                   console.log(getName);
                   console.log(aPrice);
                   $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        "productsId": getId,
                        "Name": getName,
                        'Images': getImg,
                        'Price': aPrice,
                    }

                   }).done(function(result){
                        // console.log(result);
                        $('.cart').text(result);

                   });
                });

            });
        </script>
@endsection

