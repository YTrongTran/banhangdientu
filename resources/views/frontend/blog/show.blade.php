@extends('frontend.layouts.app')
@section('menu-left')
    @include('frontend.layouts.menu-left')
@endsection
@section('content')

    <div class="blog-post-area">
        <h2 class="title text-center">Latest From our Blog</h2>
        <div class="single-blog-post">

            <h3>{{ $data['title'] }}</h3>
            <div class="post-meta">
                <ul>
                    <li><i class="fa fa-user"></i> Mac Doe</li>
                    <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                </ul>
                <span>
                    <div class="rate">
                        <div class="vote">
                            @for ($i = 1; $i <= 5; $i++)
                                @if ($i <= $ratingAvg) <input type="hidden" class="ID_blog" name="id_blog" value="{{ $data->id }}">
                                    <div class="star ratings_stars ratings_over" data-rating ="{{ $ratingAvg }}" ><input value="{{ $i }}" name="scores" type="hidden"></div>
                                @else
                                    <input type="hidden" class="ID_blog" name="id_blog" value="{{ $data->id }}">
                                    <div class="star ratings_stars" data-rating ="{{ $ratingAvg }}" ><input value="{{ $i }}" name="scores" type="hidden"></div>
                                @endif
                             @endfor
                                    <span class="rate-np">{{ $ratingAvg }}</span>

                        </div>
                    </div>
                </span>
            </div>
            <a href="">
                <img src="{{ asset('upload/Blog/' . $data['avatar']) }}" alt="">
            </a>
            <p class="contents">{!! $data['content'] !!}</p>
            <div class="pager-area">
                <ul class="pager pull-right">
                    @if ($prev)
                        <li><a href="{{ URL::to('blog/show/' . $prev) }}">Pre</a></li>
                    @endif
                    @if ($next)
                        <li><a href="{{ URL::to('blog/show/' . $next) }}">Next</a></li>
                    @endif
                </ul>
            </div>

        </div>
    </div>
    <!--/blog-post-area-->
    <div class="rating-area">
        <ul class="ratings">
            <li class="rate-this">Rate this item:</li>
            <li>
                <div class="rate">
                    <div class="vote">
                        @for ($i = 1; $i <= 5; $i++)
                            @if ($i <= $ratingAvg)
                                <input type="hidden" class="ID_blog" name="id_blog" value="{{ $data->id }}">
                                <div class="star ratings_stars ratings_over" data-rating ="{{ $ratingAvg }}" >
                                <input value="{{ $i }}" name="scores" type="hidden"></div>
                            @else
                                <input type="hidden" class="ID_blog" name="id_blog" value="{{ $data->id }}">
                                <div class="star ratings_stars" data-rating ="{{ $ratingAvg }}" >
                                <input value="{{ $i }}" name="scores" type="hidden"></div>
                            @endif
                         @endfor
                                <span class="rate-np">{{ $ratingAvg }}</span>

                    </div>
                </div>
            </li>
        </ul>


        <ul class="tag">
            <li>TAG:</li>
            <li><a class="color" href="">Pink <span>/</span></a></li>
            <li><a class="color" href="">T-Shirt <span>/</span></a></li>
            <li><a class="color" href="">Girls</a></li>
        </ul>
    </div>
    <!--/rating-area-->

    <div class="socials-share">
        <a href=""><img src="{{ asset('frontend/images/blog/socials.png') }}" alt=""></a>
    </div>
    <!--/socials-share-->

    <div class="media commnets">
        <a class="pull-left" href="#">
            <img class="media-object" src="{{ asset('frontend/images/blog/man-one.jpg') }}" alt="">
        </a>
        <div class="media-body">
            <h4 class="media-heading">Annie Davis</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
                ea commodo consequat.</p>
            <div class="blog-socials">
                <ul>
                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                </ul>
                <a class="btn btn-primary" href="">Other Posts</a>
            </div>
        </div>
    </div>

    {{-- cmt --}}
    <!--Comments-->
    <div class="CommentReply">
        <div class="response-area">
            <h2>{{ $comment->count() }} RESPONSES</h2>

            @foreach ($comment as $comment)
                <ul class="media-list">
                    <li class="media">
                        <input type="hidden" value="{{ $comment->id }}" id='id' name="id">
                        <input type="hidden" value="{{ $comment->id_blog }}" id='id_blog' name="id_blog">
                        <a class="pull-left">
                            <img class="media-object" src="{{ asset('upload/User/avatar/' . $comment->image_user) }}"
                                width="121" height="86" alt="logo">
                        </a>
                        <div class="media-body">
                            <ul class="sinlge-post-meta">
                                <li><i class="fa fa-user"></i>{{ $comment->name_user }}</li>
                                <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                            </ul>
                            <p>{{ $comment->comment }}</p>
                            <a class="btn btn-primary reply" href="#comment_form"><i class="fa fa-reply"></i>Replay

                            </a>
                        </div>

                    </li>
                </ul>
            @endforeach
        </div>
        <!--/Response-area-->
        <div class="replay-box">
            <div class="row">
                <div class="col-sm-4">
                    <h2>Leave a replay</h2>
                </div>
                <div class="col-sm-8">
                    <div class="text-area" id="comment_form">
                        <form action="{{ route('comment.store') }}" method="POST">
                            @csrf
                            <div class="blank-arrow">
                                <label>Your Name</label>
                            </div>
                            <span>*</span>
                            <input type="hidden" name="id_blog" value="{{ $data->id }}">
                            <input id="setLevel" type="hidden" value="0" name="level">
                            <textarea class="comment" name="comment" value="{{ old('comment') }}"
                                class="@error('comment') is-invalid @enderror" rows="11"></textarea>
                            @error('comment')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <button id="blogcmt" class="btn btn-primary">post comment</button>
                        </form>
                    </div>
                    <script>
                        $(document).ready(function() {
                            $("#blogcmt").click(function() {
                                var comment = "{{ Auth::check() }}";
                                var check = true;
                                if (!(comment)) {
                                    alert("Vui lòng đăng nhập");
                                    check = false;
                                } else {
                                    check;
                                }
                            });

                            $("a.reply").click(function() {
                                var getId = $(this).closest(".media-list").find(".media input").val();
                                // $(this).closest(".media-list").find(".media input#setLevel").val(getId);
                                $(this).closest(".CommentReply").find(".text-area input#setLevel").val(getId)
                                console.log(getId);
                                var level = $(this).closest(".CommentReply").find(".text-area input#setLevel").val();
                                console.log(level);
                            });
                        });
                    </script>

                    <script>
                        $(document).ready(function() {
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            //vote
                            $('.ratings_stars').hover(
                                // Handles the mouseover
                                function() {
                                    $(this).prevAll().andSelf().addClass('ratings_hover');
                                },
                                function() {
                                    $(this).prevAll().andSelf().removeClass('ratings_hover');

                                }
                            );
                            $('.ratings_stars').click(function() {
                                var url = '{{ route('rate.add') }}';
                                var rate = "{{ Auth::check() }}"
                                var check = true;
                                var rating = $(this).data('rating');
                                // console.log(rating);
                                if (!(rate)) {
                                    alert("Vui lòng đăng nhập !!!");
                                    check = false;
                                } else {
                                    var Values = $(this).find("input").val();
                                    var Id_blog = $(this).closest('.rate').find('.vote .ID_blog').val();

                                    if ($(this).hasClass('ratings_over')) {
                                        $('.ratings_stars').removeClass('ratings_over');
                                        $(this).prevAll().andSelf().addClass('ratings_over');

                                    } else {
                                        // for(var i = 1; i <= rating; i++){
                                        $(this).prevAll().andSelf().addClass('ratings_over');
                                        // }


                                    }
                                    if (Values) {
                                        $.ajax({
                                            url: url,
                                            type: 'POST',
                                            data: {
                                                'id_blog': Id_blog,
                                                'scores': Values
                                            }
                                        }).done(function(result) {
                                            console.log(result);
                                        });
                                    }
                                    check;
                                }
                            });
                        });
                    </script>

                </div>
            </div>
        </div>
    </div>

    <!--/Repaly Box-->
@endsection
