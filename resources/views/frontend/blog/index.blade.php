@extends('frontend.layouts.app')
@section('menu-left')
    @include('frontend.layouts.menu-left')
@endsection
@section('content')
    <div class="blog-post-area" >
        <h2 class="title text-center">Latest From our Blog</h2>
        @foreach ($data as $value)
            <div class="single-blog-post"  >
                <h3>{{ $value->title }}</h3>
                <div class="post-meta">

                    <ul>

                        <li><i class="fa fa-user"></i> </li>
                        <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                        <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>


                    </ul>
                    <span>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                    </span>
                </div>
                <a href="">
                    <img style="object-fit: cover" src="{{ asset('/upload/Blog/'.$value->id.'/'.$value->avatar) }}" height="572" alt="">
                </a>

                <div class="contents"  >
                    <p > 
                        {!! $value->content !!} </p>
                </div>
                <a class="btn btn-primary" href="{{ route('blog_details.show', ['id' => $value->id]) }}">Read More</a>
            </div>
        @endforeach
        <div class="pagination-area">
            <ul class="pagination">
                {{-- <li><a href="" class="active">1</a></li>
                <li><a href="">2</a></li>
                <li><a href="">3</a></li> --}}
                <li></i>{{ $data->links() }}</li>
            </ul>
        </div>


    </div>



@endsection
