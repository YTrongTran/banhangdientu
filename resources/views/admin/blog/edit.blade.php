@extends('admin.layouts.app')
@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    @if (session('success'))
        <div class="alert alert-success alert-dismissible">
            <button class="close" data-dismiss="alert" aria-hidden="true" type="button">X</button>
            <h4><i class="icon fa fa--check"></i>Thông báo!</h4>
            {{ session('success') }}
        </div>
    @endif
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{ $title }}</h4>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex align-items-center justify-content-end">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card card-body">
                    <h4 class="card-title">Forms {{ $title }}</h4>
                    {{-- <h5 class="card-subtitle"> All bootstrap element classies </h5> --}}
                    <form class="form-horizontal m-t-30" action="" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Title <span style="color: red">(*)</span></label>
                            <input type="text" value="{{ $editId->title }}" class="form-control" name="title"
                                class="@error('title') is-invalid @enderror" placeholder="Title">

                        </div>
                        @error('title')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <div class="form-group">
                            <label>Images</label>
                            <input type="file" class="form-control" value="{{ $editId->avatar }}" class="@error('avatar') is-invalid @enderror"
                                name="avatar">
                                <p><img src="{{ asset('upload/Blog/'.$editId->id.'/'. $editId->avatar) }}" alt="" width="100" height="100"></p>

                        </div>
                        @error('avatar')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror

                        <div class="form-group">
                            <label>Description</label>
                            <div class="form-group">
                                <textarea class="form-control" rows="5"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Content</label>
                            <div class="form-group">
                                <textarea class="form-control" value="{{ $editId->content  }}" class="@error('content') is-invalid @enderror" id="blog1"
                                    rows="5" name="content">{{ $editId->content }}</textarea>
                            </div>
                        </div>
                        @error('content')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <button type="submit" name="submit" class="btn btn-success">Edit Blog</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        All Rights Reserved by Nice admin. Designed and Developed by
        <a href="https://wrappixel.com">WrapPixel</a>.
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
@endsection
