@extends('admin.layouts.app')
@section('content')
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">{{ $title }}</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{ route('admin.dashboard') }}">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"><strong>Table Blog</strong></h4>
                                {{-- <h6 class="card-subtitle">Similar to tables, use the modifier classes .thead-light to make <code>&lt;thead&gt;</code>s appear light.</h6> --}}
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Title</th>
                                            <th scope="col">Images</th>
                                            <th scope="col">Content</th>
                                            <th scope="col">active</th>
                                            <th> <a href="{{ route('blog.add') }}" style="color: green;">Add
                                                <i class="m-r-10 mdi mdi-account-multiple-plus"></i>
                                              </a>
                                            </th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                        @php
                                             $i=1;
                                        @endphp
                                        @foreach ($array as $value)
                                            <tr>
                                            <td scope="row">{{ $i }}</td>
                                            <td>{{ $value->title }}</td>
                                            <td class="img">
                                                <img style="object-fit: cover" src="{{ asset('upload/Blog/'.$value->id.'/'. $value->avatar) }}" alt="anh" width="100" height="100">
                                            </td>
                                            <td class="content" style="height: 100px; width: 100%; display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp:3; overflow: hidden; text-overflow: ellipsis ">{!! $value->content !!}</td>
                                            <td>
                                                <a href="{{ route('blog.edit',['id'=>$value->id]) }}" style="margin-right: 20px;">
                                                    Edit
                                                <i class="mdi mdi-account-edit"></i>
                                              </a>
                                              <a href="{{ route('blog.delete',['id'=>$value->id]) }}" style="color: red; margin-right: 20px;">Delete
                                                <i class="mdi mdi-delete-sweep"></i>
                                              </a>
                                              {{-- <a href="" style="color: rgb(30, 255, 0); margin-right: 20px;">List
                                                <i class="mdi mdi-format-list-bulleted"></i>
                                              </a> --}}
                                            </td>
                                        </tr>
                                        <?php $i++ ;?>
                                        @endforeach
                                    </tbody>

                                </table>
                            </div>
                            {!! $array->links() !!}
                        </div>

                    </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Nice admin. Designed and Developed by
                <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
@endsection
