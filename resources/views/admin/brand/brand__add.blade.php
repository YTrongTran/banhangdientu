@extends('admin.layouts.app')
@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">{{ $title }}</h4>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex align-items-center justify-content-end">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card card-body">
                @if (session('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button class="close" data-dismiss="alert" aria-hidden="true" type="button">X</button>
                        <h4><i class="icon fa fa--check"></i>Thông báo!</h4>
                        {{ session('success') }}
                    </div>
                @endif

                <form class="form-horizontal m-t-30" method="POST" action="{{ route('brand.add') }}">
                    @csrf

                    <div class="form-group">
                        <label for="example-email">Name Brand <span style="color: red">(*)</span> <span class="help"></span></label>
                        <input type="text" name="name" value="{{ old('name') }}"  class="form-control" class="@error('name') is-invalid @enderror"  placeholder="Name">
                    </div>
                    <p class="help is-danger" style="color: red;">{{ $errors->first('name') }}</p>
                    <button type="submit" class="btn btn-success">Create Brand</button>
                    <a type="submit" href="{{ route('brand.index') }}" class="btn btn-success">View Brand</a>
                </form>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<footer class="footer text-center">
    All Rights Reserved by Nice admin. Designed and Developed by
    <a href="https://wrappixel.com">WrapPixel</a>.
</footer>
@endsection
