<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('products', function (Blueprint $table) {
            $table->String('name')->after('id');
            $table->double('price')->after('name');
            $table->BigInteger('id_category')->unsigned()->after('price');
            $table->foreign('id_category')->references('id')->on('categories');
            $table->BigInteger('id_brand')->unsigned()->after('id_category');
            $table->foreign('id_brand')->references('id')->on('brands');
            $table->unsignedInteger('status')->after('id_brand')->default('0')->comment='1:sale 0:new';
            $table->unsignedInteger('sale')->after('status')->default('0');
            $table->String('company')->after('sale');
            $table->String('hinhanh')->after('company');
            $table->String('detail')->after('hinhanh');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('products');
    }
}
