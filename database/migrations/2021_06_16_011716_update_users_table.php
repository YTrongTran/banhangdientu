<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->String('phone')->after('password')->nullable();
            $table->String('address')->after('phone')->nullable();
            $table->BigInteger('id_country')->unsigned()->after('address');
            $table->foreign('id_country')->references('id')->on('countries');
            $table->String('avatar')->after('id_country')->nullable();
            $table->unsignedInteger('level')->after('remember_token')
            ->default(1)->comment='1:admin 0:member';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('users');
    }
}
