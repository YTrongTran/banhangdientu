<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogCmtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_cmts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('image_user');
            $table->String('name_user');
            $table->String('comment');
            $table->BigInteger('id_blog')->unsigned();
            $table->foreign('id_blog')->references('id')->on('blogs');
            $table->BigInteger('id_user')->unsigned();
            $table->foreign('id_user')->references('id')->on('users');
            $table->unsignedInteger('level');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_cmts');
    }
}
