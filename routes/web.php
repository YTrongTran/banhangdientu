<?php

use App\Models\Blog;
use Intervention\Image\Facades\Image;
use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     return view('welcome');
     // $img = Image::make('https://cdn.pixabay.com/photo/2015/09/09/16/05/forest-931706_960_720.jpg')->resize(300, 200);

     // return $img->response('jpg');
});

Route::group([
    // 'prefix' => 'frontend',
    'namespace' => 'Frontend',
], function () {
    //home

    Route::group([
        'prefix' => 'HomeProducts',
    ],function(){
        Route::get('/index','HomeProductsController@index')->name('homeproducts.index');
        Route::post('/getProductsAjax', 'HomeProductsController@getProductsAjax')->name('ProductAjax.getProductsAjax');
        Route::get('/ProductsDetails/{id}','HomeProductsController@show')->name('details.show');
        //search
        Route::get('/searchProducts', 'HomeProductsController@viewsearch')->name('searchProducts.textsearch');
     

    });

    // cart
        Route::group([
            'prefix' => 'Cart',
        ], function(){

            Route::get('showCart','CartController@index')->name('cart.show');
            Route::post('AddToCartAjax','CartController@addToCart')->name('cart.addTocart');
            Route::post('DownToCartAjax','CartController@downToCart')->name('cart.downTocart');
            Route::post('DeleteCartAjax','CartController@deleteToCart')->name('cart.deleteajax');
            Route::get('checkout','CartController@checkout')->name('cart.checkout');
            // đăng ký nhanh
            Route::post('singup_now','CartController@sign_up_now')->name('cart.register');
            Route::post('sing_up','CartController@sing_up')->name('cart.sing_up');
            //gửi gmail
            Route::get('/testmail', 'MailController@testMail');
            Route::post('/testmail', 'MailController@testMail')->name('checkout');
        });


    Route::group([
        'prefix' => 'blog',
    ], function () {
        Route::get('/index', 'BlogController@index')->name('fdblog.index');
        Route::get('/blog-detail/{id}', 'BlogController@show')->name('blog_details.show');
        //comment
        Route::post('/comment','BlogCmtController@store')->name('comment.store');
        // Rate
        Route::get('/diemtrungbinh', 'RateController@index');
        Route::post('/rate', 'RateController@store')->name('rate.add');
    });

    Route::group([
        'prefix' => 'products',
    ],function(){
        Route::get('/index', 'ProductController@index')->name('products.index');
        Route::get('/create', 'ProductController@create');
        Route::post('/create', 'ProductController@store')->name('products.add');

        Route::get('/edit/{id}', 'ProductController@edit')->name('products.edit');
        Route::post('/edit/{id}', 'ProductController@update')->name('products.edit');
        Route::get('delelete/{id}', 'ProductController@destroy')->name('products.delete');

    });

    Route::group([
        'prefix' =>'member',
    ],function(){
            Route::get('/index','MemberController@index')->name('account.index');
            // đăng ký của member
            Route::get('/register','MemberController@create')->name('member.register');
            Route::post('/register','MemberController@store')->name('member.register');
            // account
            Route::get('/account/edit/{id}','MemberController@edit')->name('account.edit');
            Route::post('/account/edit/{id}','MemberController@update')->name('account.edit');

        Route::get('/login', 'LoginRemberController@getLogin')->name('login.dangnhap');
        Route::post('/login', 'LoginRemberController@postLogin')->name('login.dangnhap');
        Route::get('/logout','LoginRemberController@getLogout')->name('member.logout');
        Route::post('/logout','LoginRemberController@getLogout')->name('member.logout');

    });

});


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',

], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');
    Route::group([
        'prefix'=> 'user',
    ],function (){
        Route::get('/index', 'UserController@index')->name('user.view');
        Route::get('/edit/{id}', 'UserController@edit')->name('user.edit');
        Route::post('/edit/{id}', 'UserController@update')->name('user.edit');
    });

    Route::group([
        'prefix' => 'country',
    ],function(){
        Route::get('/index', 'CountryController@index')->name('country.index');
        Route::get('/create', 'CountryController@create');
        Route::post('/create', 'CountryController@store')->name('country.add');
        Route::get('/edit/{id}', 'CountryController@edit')->name('country.edit');
        Route::post('/edit/{id}', 'CountryController@update')->name('country.edit');
        Route::get('/delelete/{id}', 'CountryController@destroy')->name('country.delete');
    });

    Route::group([
        'prefix' => 'category',
    ],function(){
        Route::get('/index', 'CategoryController@index')->name('category.index');
        Route::get('/create', 'CategoryController@create');
        Route::post('/create', 'CategoryController@store')->name('category.add');
        Route::get('/edit/{id}', 'CategoryController@edit')->name('category.edit');
        Route::post('/edit/{id}', 'CategoryController@update')->name('category.edit');
        Route::get('/delelete/{id}', 'CategoryController@destroy')->name('category.delete');
    });

    Route::group([
        'prefix' => 'brand',
    ],function(){
        Route::get('/index', 'BrandController@index')->name('brand.index');
        Route::get('/create', 'BrandController@create');
        Route::post('/create', 'BrandController@store')->name('brand.add');
        Route::get('/edit/{id}', 'BrandController@edit')->name('brand.edit');
        Route::post('/edit/{id}', 'BrandController@update')->name('brand.edit');
        Route::get('delelete/{id}', 'BrandController@destroy')->name('brand.delete');
    });





    Route::group([
        'prefix' => 'blog',
    ],function(){
        Route::get('/index', 'BlogController@index')->name('blog.index');

        Route::get('/add', 'BlogController@create')->name('blog.add');
        Route::post('/add', 'BlogController@store')->name('blog.add');
        Route::get('/blog/{id}', 'BlogController@edit')->name('blog.edit');
        Route::post('/blog/{id}', 'BlogController@update')->name('blog.edit');
        Route::get('/delete/{id}', 'BlogController@destroy')->name('blog.delete');
    });
});

// Route::prefix('admin')->group(function () {
//     Route::get('/dashboard', 'Admin\DashboardController@index')->name('admin.dashboard');

//     Route::prefix('/user')->group(function () {
//         Route::get('/index', 'Admin\UserController@index')->name('user.view');
//         Route::get('/edit/{id}', 'Admin\UserController@edit')->name('user.edit');
//         Route::post('/edit/{id}', 'Admin\UserController@update')->name('user.edit');
//     });
//     Route::prefix('/country')->group(function () {
//         Route::get('/index', 'Admin\CountryController@index')->name('country.index');
//         Route::get('/create', 'Admin\CountryController@create');
//         Route::post('/create', 'Admin\CountryController@store')->name('country.add');
//         Route::get('/edit/{id}', 'Admin\CountryController@edit')->name('country.edit');
//         Route::post('/edit/{id}', 'Admin\CountryController@update')->name('country.edit');
//         Route::get('/delelete/{id}', 'Admin\CountryController@destroy')->name('country.delete');
//     });
//     Route::prefix('/blog')->group(function () {
//         Route::get('/index', 'Admin\BlogController@index')->name('blog.index');
//         Route::get('/add', 'Admin\BlogController@create')->name('blog.add');
//         Route::post('/add', 'Admin\BlogController@store')->name('blog.add');
//         Route::get('/blog/{id}', 'Admin\BlogController@edit')->name('blog.edit');
//         Route::post('/blog/{id}', 'Admin\BlogController@update')->name('blog.edit');
//         Route::get('/delete/{id}', 'Admin\BlogController@destroy')->name('blog.delete');
//     });
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');