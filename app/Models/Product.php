<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = [
      'id', 'name','price','id_category',
        'id_brand','status','sale','company','hinhanh','detail'
    ];
}
