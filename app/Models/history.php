<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class history extends Model
{
    //
    protected $table = 'histories';
    protected $fillable = [
        'name', 'phone','email','id_user','price'
    ];
}
