<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCmt extends Model
{

    protected $table = 'blog_cmts';
    protected $fillable = [
        'id','image_user','name_user','comment','id_blog','id_user','level'
    ];
}
