<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Category';
        // $array = DB::table('categories')->paginate(3);
        $array = Category::paginate(3);
        return view('admin.category.category__view',compact(['title','array']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $title = "Add Category";
        return view('admin.category.category__add',compact(['title']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        //
        $category = new Category();
        $data = $request->all();
        // dd($data);
        $category->name = $request->name;
        if($category->save($data)){
            return redirect('/admin/category/create')->with('success',__('Bạn thêm thành công'));
        }else{
            return redirect('/admin/category/create')->withErrors('Bạn thêm thất bại');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category,$id)
    {
        //
        $title = 'Edit Category';
        $editCategory = Category::find($id);
        // dd($editCategory);
        return view('admin.category.category__edit',compact([
            'title','editCategory'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        //
        $edit_Category = Category::find($request->id);
        $edit_Category->name = $request->name;
        // dd($edit_Category);
        if($edit_Category->update()){
            return redirect()->back()->with('success',__('Update thành công'));
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category,Request $request)
    {
        //
        $delete = Category::destroy($request->id);
        return redirect('/admin/category/index');
    }
}
