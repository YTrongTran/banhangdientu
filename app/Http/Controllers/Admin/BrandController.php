<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BrandRequest;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BrandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = "Brand";
        $array = DB::table('brands')->paginate(3);
        // dd($array);
        return view('admin.brand.brand__view',compact(['title','array']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $title = 'Add Brand';
        return view('admin.brand.brand__add',compact(['title']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BrandRequest $request)
    {
        //
        $brand = new Brand();
        $data = $request->all();

        $brand->name = $request->name;
        if($brand->save($data)){
            return redirect()->back()->with('success', __('Bạn thêm thành công'));
        }else{
            return redirect()->back()->withErrors('Bạn thêm thất bại');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand,Request $request,$id)
    {
        //
        $title = "Edit Brand";

        $EditBrand  = Brand::find($id);

        return view('admin.brand.brand__edit',compact([
            'title', 'EditBrand',
        ]));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(BrandRequest $request, Brand $brand,$id)
    {
        //
        $editBrand  = Brand::find($id);
        $data = $request->all();
        $editBrand->name = $request->name;
        if($editBrand->update($data)){

            return redirect()->back()->with('success', __('Bạn sửa thành công'));
        }else{
            return redirect()->back()->withErrors(('Bạn sửa thất bại'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand,Request $request,$id)
    {
        //
        $delete = Brand::destroy($request->id);

        return redirect('/admin/brand/index');
    }
}
