<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CountryRequest;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $title = 'Country';
        // $array = Country::all();
        $array = Country::paginate(3);
        return view ('admin.country.view',compact(['title','array']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //lấy form
        $title = 'Thêm mới Country';
        return view('admin.country.add',compact(['title',]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountryRequest $request)
    {
        //gọi Model Counry ra.
        $country = new Country();
        $country['name'] = $request->name;
        //$country->save();
        if($country->save()){
            return redirect('admin/country/index')->with('success',__('Thêm thành công'));
        }else{
            return redirect()->back()->withErrors(('Thêm thành bại'));
        }
        //return redirect('admin/country/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country, Request $request)
    {
        //
        $title ='Edit Country';
        //lấy id.
        $Uscountry = Country::find($request->id);
       // dd($Uscountry);
        return view('admin.country.edit',compact(['title','Uscountry']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data =Country::find($request->id);
        $data['name'] = $request->name;
        //$country->save();
        if($data->update()){
            return redirect('admin/country/index')->with('success',__('Edit thành công'));
        }else{
            return redirect()->back()->withErrors(('Edit thành bại'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country, Request $request)
    {
        //xoá
        $delete = Country::find($request->id);
        $delete->delete();
        return redirect('admin/country/index');

    }
}
