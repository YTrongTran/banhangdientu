<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogRequest;
use App\Models\Blog;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class BlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     //SELECT * FROM `blogs` INNER JOIN users ON  blogs.id = users.id WHERE users.email = 'trantrongycntt@gmail.com'
        $title = 'Blog Index';
        $array = DB::table('blogs')->paginate(3);
      //  dd($array);
       //  $paig = Blog::paginate(4);
        return view('admin.blog.view',compact(['title','array']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Blog Create';
        return view('admin/blog/add',compact(['title']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        //
        $data = new Blog();
        $data->title = $request->title;
        $file = $request->avatar;
        if(!empty($file)){
            $data->avatar = $file->getClientOriginalName();
        }
        $data->content = $request->content;

        if($data->save()){
            if(!empty($file)){
                $file->move('upload/Blog/'.$data->id,$file->getClientOriginalName());
            }
            return redirect('/admin/blog/index')->with('success', __('Create Blog success'));
        }else{
            return redirect()->withError('Create Blog Error');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog,$id)
    {
        //SELECT * FROM `blogs` INNER JOIN users ON  blogs.id = users.id WHERE users.email = 'trantrongycntt@gmail.com'
        //->join('users','users.id','=','blogs.id')->select('blogs.*', 'blogs.title','blogs.avatar','blogs.content','users.name','users.email')
        // $emailUsers = User::find($id);
        // $id_user = DB::table('blogs')->join('users','users.id','=','blogs.id')->select('blogs.*', 'blogs.title','blogs.avatar','blogs.content','users.name','users.email')->where('users.email',$emailUsers->email)->get();
        // dd($id_user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog, $id)
    {
        //
        $title = "Edit Blog";
        $editId = Blog::find($id);
      //  dd($editId->content);
        return view('admin.blog.edit',compact(['editId','title']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(BlogRequest $request, Blog $blog)
    {
        //
        $blog = Blog::find($request->id);
        $data = $request->all();
        $file = $request->avatar;
        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }

        if($blog->update($data)){
            if(!empty($file)){
               $file->move('upload/Blog/'.$blog->id,$file->getClientOriginalName());
            }
            return redirect('admin/blog/index')->with('success',__('Edit thành công'));
        }else{
            return redirect()->back()->withErrors('Edit thất bại');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog,Request $request)
    {
        //
        $delete = Blog::find($request->id);
        $delete->delete();
        return redirect('admin/blog/index');
        //
}
}