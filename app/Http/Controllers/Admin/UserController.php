<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UploadProfileRequest;
use App\Models\Country;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       return view('admin.user.user-profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request , $id)
    {
        //lấy tất cả thông tin;
    //    $data = User::find($request->id);
       // $country = DB::table('users')->join('countries', 'countries.id','=','users.id_country')->get();
       $country = DB::table('countries')->pluck('name','id');
     //  dd( $country);
        // $id = $request->id;
        return view('admin.user.user-profile',compact([
            'country',
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UploadProfileRequest $request, $id)
    {
        //
        $userId = Auth::id(); //nó lấy di từ admin.
        $users = User::findOrFail($userId); //lấy tất cả danh sách có id = mấy.

        $array = $request->all(); //dùng request lấy tất cả tên giá trị trong form

        $file = $request->avatar; //hiện tại chưa có thì báo null.

        if(!empty($file)){
            $array['avatar']  = $file->getClientOriginalName();
        }
        //chổ ni kiểm tra password nghĩa là nó ko nhập thì giữ nguyên
        if($array['password']){
            $array['password'] = bcrypt($array['password']);
        }else{
            $array['password'] = $users->password;
        }

        if($users->update($array)){
            if(!empty($file)){
                //lưu ảnh
                 $file->move('upload/User/avatar/'.$users->id,$file->getClientOriginalName());
            }
            return redirect()->back()->with("success", __("Upadte profile Success"));
        //  return 'oke';
        }else{
            return redirect()->back()->withErrors('Update profile Error');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
