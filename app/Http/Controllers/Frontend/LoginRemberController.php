<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRemeberRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoginRemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function getLogin(){
        //lý do ta thêm tại thằng login và register nó nằm trong 1
        //from đâm ra chúng ta viết hàm country để select lấy giá trị đó,
        $country = DB::table('countries')->pluck('name','id');
        return view('frontend.member.profile-frontend',compact(['country']));
    }
    public function postLogin(LoginRemeberRequest $request){
        $arrayLogin = [
            'email' =>$request->email,
            'password' =>$request->password,
            'level'=> 0,
        ];
        $remeber = false;

        if($request->remember_me){
            $remeber = true;
        }

        if(
            Auth::attempt($arrayLogin, $remeber)){
                return redirect('/member/account/edit/'.Auth::user()->id)->with('success',__('Đăng nhập thành công!!!'));
            // dd('đăng nhập thành công');
        }else{
            // dd('đăng nhập thất bại');
            return redirect()->back()->with('success',__("Email or Password sai"));
        }
    }

    public function getLogout(){
        Auth::logout();
        return redirect('/member/login');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
