<?php

namespace App\Http\Controllers\Frontend;

use App\Models\BlogCmt;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommentBlog;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogCmtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(CommentBlog $request)
    {
            $array = new BlogCmt();
            if(Auth::id() == null){
                $array->id_user = "";
            }else{
                $array->id_user = Auth::id();
                $array->id_blog = $request->id_blog;
                $array->image_user = Auth::user()->avatar;
                $array->name_user =  Auth::user()->name;
                $array->comment = $request->comment;
                if($array->level){
                    $array->level = 0;
                }else{
                    $array->level = $request->level;
                }
                $array->save();
            }

        return back();
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\BlogCmt  $blogCmt
     * @return \Illuminate\Http\Response
     */
    public function show(BlogCmt $blogCmt)
    {
        //

        // return view('frontend.blog.show',compact(['comment']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlogCmt  $blogCmt
     * @return \Illuminate\Http\Response
     */
    public function edit(BlogCmt $blogCmt)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BlogCmt  $blogCmt
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlogCmt $blogCmt)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlogCmt  $blogCmt
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlogCmt $blogCmt)
    {
        //
    }
}
