<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Brand;
use App\Models\category;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
class HomeProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $products = Product::orderBy('created_at','DESC')->paginate(12);

        // dd($products);
        // $get = Product::all()->toArray();
        // $array = json_decode($get['hinhanh']);
        // var_dump($array);
        return view('frontend.homeProducts.index',compact(['products']));
    }

    public function getProductsAjax(Request $request){
        if($request->ajax()){
            $id = $request->productsId;
            $data = Product::find($id);
            $check = true;
            $array = session()->get('cart');
            if(!empty($id)){
                $child = [];
                    $idChild = $data['id'];
                    $name = $data['name'];
                    $price = $data['price'];
                    $sale = $data['sale'];
                    $priceSale = $price*((100-$sale)/100);
                    $hinhanh = $data['hinhanh'];
                    $quantity = 1;
                $child['id']= $idChild;
                $child['name']= $name;
                $child['price']= $priceSale;
                $child['hinhanh']= $hinhanh;
                $child['quantity']= $quantity;
                $child['total']=  $quantity * $priceSale ;
               if(session()->has('cart')){
                $getSession = session()->get('cart');
                // print_r($getSession);
                foreach($getSession as $key => $value){
                    if($id == $value['id']){
                        $getSession[$key]['quantity'] = $getSession[$key]['quantity']  + 1;
                        $getSession[$key]['total'] = $getSession[$key]['quantity']  * $getSession[$key]['price'] ;
                        session()->put('cart', $getSession);
                        $check = false;
                    }
                }
               }
                if($check){
                    $array[$id] = $child;
                    session()->put('cart' ,$array);
                }
                echo(count(Session('cart')))  ;
                // echo "<pre>";
                // print_r(session()->get('cart'));
                // Session::flash('success', 'thêm vào giỏ hàng thành công !!!');
                //   return response()->json(['msg' => 'thêm vào giỏ hàng thành công !!!'], 200);
            }

        }

    }

    public function viewsearch(Request $request){
        // dd($request->query);
        if($request->get('query')){
            $query = $request->get('query');
            $data = Product::where('name', 'LIKE', '%'.$query .'%')->get();
        }
            
        return view('frontend.searchAjax',compact(['data'])); 
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $brand = Brand::pluck('name','id');
        $category = category::pluck('name','id');
        $details = Product::find($id);
     
        
      
         // dd($brand[$details->id_brand]);
        return view('frontend.homeProducts.productsDetali',compact(['details','brand','category']));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}