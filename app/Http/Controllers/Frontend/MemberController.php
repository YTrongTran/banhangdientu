<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Controllers\Controller;
use App\Http\Requests\AccountRequest;
use App\Http\Requests\MemberLoginRequest;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('frontend.member.account');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $country = DB::table('countries')->pluck('name','id');
       // dd($country);
        return view('frontend.member.profile-frontend',compact(['country']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberLoginRequest $request)
    {
        // 'name', 'email', 'password', 'phone','address','id_country','avatar', 'level'
        $member  = New User();
        $member->name = $request->name;
        $member->email = $request->email;
        $member->phone = $request->phone;
        $member->address = $request->address;
        $member->level = 0;
        $file = $request->avatar;
        $member->password =bcrypt($request->password);
        $member->id_country = $request->id_country;
        if(!empty($file)){
            $member['avatar'] = $file->getClientOriginalName();
        }

        if($member->save()){
            if(!empty($file)){
                // $member['avatar'] = $file->getClinetOriginalName();
                $file->move('upload/User/avatar/'.$member->id,$file->getClientOriginalName());
            }
            return redirect('/member/login')->with("success",__("Đăng ký thành công"));
            // dd('Đăng ký thành công');
        }else{
            // dd('Đăng ký thất bại');
            return redirect('member/register')->withErrors("Đăng ký thất bại");
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $country = DB::table('countries')->pluck('name','id');
        $member = User::find($id);

        return view('frontend.member.account-colsm6',compact(['country','member']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $membersID = Auth::id();
        $mber = User::findOrFail($membersID);
        $data = $request->all();
        $mber['id_country'] = $request->id_country;

        $file = $request->avatar;

        if(!empty($file)){
            $data['avatar'] = $file->getClientOriginalName();
        }

        if($data['password']){
            $data['password'] = bcrypt($request->password);
        }else{
            $data['password'] = $mber->password;
        }


        if($mber->update($data)){
            if(!empty($file)){
                $file->move('upload/User/avatar/'.$mber->id,$file->getClientOriginalName());
            }
            return back()->with("message","Cập nhật thành công");
        }else{
            return back()->withErrors('Cập nhật không thành công');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
