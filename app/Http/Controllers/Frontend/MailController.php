<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CheckoutRequest;
use App\Models\history;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;


class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function testMail(CheckoutRequest $request)
    {
        //
        if(empty(Auth::id())){
            return redirect()->back()->with('error','Bạn vui lòng đăng nhập để tiếp tục thực hiện đặt hàng ?');
        }else{
            $sum = 0;
            $getSession = session()->get('cart');
            foreach($getSession as $key => $value){
                $sum += $getSession[$key]['total'];
            }
            $history = new history();
            $history->id_user = Auth::id();
            $history->name = $request->name;
            $history->email = $request->email;
            $history->phone = $request->phone;
            $history->price =  ceil($sum);

            $user = User::find(Auth::id());
            $user->address = $request->address;
            $user->update();
            $history->save();
            $order_email = Auth::user()->email;
            $order_name = Auth::user()->name;
            //thời gian checkout
            $now = Carbon::now('Asia/Ho_Chi_Minh')->format('d-m-Y H:i:s');
            Mail::send('frontend.sendmail.mailForm', compact(['getSession','sum','order_email','order_name','now']), function ($message) use ($order_email,$order_name) {
                $message->subject('Email đặt hàng thành công');
                $message->to($order_email,$order_name);
                $message->from('shopthuongmaiwebiste@gmail.com');
            });
            $request->session()->forget('cart');
            return redirect('/Cart/checkout/')->with('success',__('Cảm ơn bạn vui lòng kiểm tra hộp thư Email của mình !!!'));
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}