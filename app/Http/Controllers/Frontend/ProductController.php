<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductFrontendsRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $productsList = Product::paginate(5);

        return view('frontend.products.products__view', compact(['productsList']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $id_category = Category::all()->pluck('name','id');
        $id_brand = Brand::all()->pluck('name','id');

        return view('frontend.products.products__add', compact([
            'id_category', 'id_brand'
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductFrontendsRequest $request)
    {

        $data = $request->all();
        $products = new Product();
        $products->name = $data['name'];
        $products->price = $data['price'];
        $products->id_category = $data['id_category'];
        $products->id_brand = $data['id_brand'];
        $products->status = $data['status'];
        $products->sale = $data['sale'];
        $products->company = $data['company'];
        $products->detail = $data['detail'];

        if ($request->hasFile('hinhanh')) {
            foreach ($request->file('hinhanh') as $image) {
                //lấy tên ảnh lưu vào biến name
                $name = $image->getClientOriginalName();
                $name_2 = 'hinh85_' . $image->getClientOriginalName();
                $name_3 = 'hinh329_' . $image->getClientOriginalName();
                $array[] = $name;
            }

            if (count($array) > 3) {
                return back()->with('message', 'Chỉ tiêu tối đa 3 ảnh');
            } else {
                $products->hinhanh = json_encode($array);
            }
        }

        //

        if ($products->save($data)) {
            $id = $products->id;
            $namePath = public_path('upload/products/' . $id);
            foreach ($request->file('hinhanh') as $image) {
                $name = $image->getClientOriginalName();
                $name_2 = 'hinh85_' . $image->getClientOriginalName();
                $name_3 = 'hinh329_' . $image->getClientOriginalName();
                if (!is_dir($namePath)) {
                    mkdir($namePath, 0777, true);
                }
                $path = $namePath . '/' . $name;
                $path_2 = $namePath . '/' . $name_2;
                $path_3 = $namePath . '/' . $name_3;
                //tên đường dẫn được lưu trữ.
                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize('85', '84')->save($path_2);
                Image::make($image->getRealPath())->resize('329', '380')->save($path_3);

            }

            return back()->with('message', 'Bạn thêm thành công');
        } else {
            return back()->withErrors('Bạn thêm thất bại');
        }

    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $idProducts = Product::find($id);
        $getProductsImg = Product::find($id)->toArray();
        $getImg = json_decode($getProductsImg['hinhanh'], true);
        $id_category = Category::pluck('name','id');
        $id_brand = Brand::pluck('name','id');
        return view('frontend.products.products__edit', compact([
            'idProducts', 'id_category', 'id_brand', 'getImg',
        ]));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $editProductsImg = Product::find($id)->toArray();
        $arr = json_decode($editProductsImg['hinhanh'], true);
        $input = $request->all();
        $editProducts = Product::find($id);
        $editProducts->name = $input['name'];
        $editProducts->price = $input['price'];
        $editProducts->id_category = $input['id_category'];
        $editProducts->id_brand = $input['id_brand'];
        $editProducts->status = $input['status'];
        $editProducts->sale = $input['sale'];
        $editProducts->company = $input['company'];
        $editProducts->detail = $input['detail'];
        //kiểm tra hình cũ có trùng với hình ảnh checkbox không.
        if (!empty($arr)) {
            if (!empty($_POST['array'])) {
                $OldImages = array_diff($arr, $_POST['array']); //ảnh còn lại
                $editProducts['hinhanh'] = json_encode($OldImages);

            }
        }
        if ($request->hasFile('hinhanh')) {
            foreach ($request->file('hinhanh') as $img) {
                $name = $img->getClientOriginalName();
                $name_2 = "hinh85_" . $img->getClientOriginalName();
                $name_3 = "hinh329_" . $img->getClientOriginalName();

                $path = public_path('upload/products/' . $editProducts->id . '/' . $name);
                $path_2 = public_path('upload/products/' . $editProducts->id . '/' . $name_2);
                $path_3 = public_path('upload/products/' . $editProducts->id . '/' . $name_3);

                Image::make($img->getRealPath())->save($path);
                Image::make($img->getRealPath())->resize('85', '84')->save($path_2);
                Image::make($img->getRealPath())->resize('329', '380')->save($path_3);
                $data[] = $name;
            }
            // hinh cu + hinh upload < 3
            if(count($arr +$data) > 3 || count($arr) > 3 || count($data) > 3){
                return back()->with('message', 'Chỉ tiêu tối đa 3 ảnh');
            }else{
                $editProducts['hinhanh'] = json_encode(array_merge($arr,$data));
            }
            //hinh cu con lai + hinh upload <= 3
            if (!empty($_POST['array'])) {
                $OldPhoto = array_diff($arr, $_POST['array']); //ảnh còn lại
                if(count($OldPhoto + $data) > 3 || count($OldPhoto) > 3 || count($data) > 3){
                    return back()->with('message', 'Vui lòng xóa ảnh trước');
                }else{
                    $editProducts['hinhanh'] = json_encode(array_merge($OldPhoto,$data));
                }
            }
        }

        if ($editProducts->update()) {
            return back()->with('message', 'Update thành công');
        } else {
            return back()->withErrors('Update thất bại');
        }
        // die;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $deleteProducts = Product::destroy($id);
        // dd($deleteProducts);
        return redirect('products/index');
    }
}
