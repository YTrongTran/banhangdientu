<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCmt;
use App\Models\Rate;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Blog::paginate(3);
            //  $viewEmail = DB::table('blogs')->join('users','users.id','=','blogs.id')->select('blogs.*','blogs.title','blogs.avatar','blogs.content','users.name','users.email')->where('users.email', 'trantrongycntt@gmail.com')->get();
            // dd($viewEmail->name);
        return view('frontend.blog.index',compact(['data']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function blog_details(){
        return view('frontend.blog.blog-details');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

            $data = Blog::find($id);

            $geID = BlogCmt::find($id);
            $id_blog = $id;
            $comment = BlogCmt::all()->where('id_blog',$id_blog);
            $prev = Blog::where('id', '<', $data->id)->max('id');
            $next = Blog::where('id','>',$data->id)->min('id');
            // dd($id_blog);
            //hiển thị điểm trung bình.
            //SELECT id_blog, AVG(scores) FROM rates WHERE id_blog = 2

             $scores = Rate::all()->where('id_blog',$id_blog)->avg('scores');
             $ratingAvg = round($scores);
        //   dd($data);
         return view('frontend.blog.blog-details',compact(['data','comment','geID','prev','next','ratingAvg']));
    }


    // public function postComment($id,Request $request){

    //
    // }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // $data = Blog::find($id);
        // return view ('frontend.blog.show',compact(['data']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
