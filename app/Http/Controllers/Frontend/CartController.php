<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginCheckoutRequest;
use App\Http\Requests\MemberLoginRequest;
use App\Http\Requests\SingsnowRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('frontend.cart.cartShow');
    }
    public function addToCart (Request $request){
        $getId = $request->getID;
        $Total = 0;
        //kiểm tra trong session có giỏ hàng chưa
        if(session()->has('cart')){
            $ajaxSession = session()->get('cart');
            foreach($ajaxSession as $key => $value){
                if($getId == $value['id']){
                    $ajaxSession[$key]['quantity'] =  $ajaxSession[$key]['quantity'] + 1;
                    $ajaxSession[$key]['total'] =  $ajaxSession[$key]['quantity'] *  ceil($ajaxSession[$key]['price']);

                }
            }
            session()->put('cart', $ajaxSession);
            //tinh tong
            // foreach($ajaxSession as $key => $value){
            //     $Total += $ajaxSession[$key]['quantity'] * ceil($ajaxSession[$key]['price']);
            // }
            // echo "<pre>";
            //  var_dump($getId);

        }
    }
    public function downToCart (Request $request){
        $getIdDown = $request->getDownId;
        // $check =true;
        if(session()->has('cart')){
            $getsession = session()->get('cart');
            foreach($getsession as $key => $value){
                if($getIdDown == $value['id']){
                    if( $getsession[$key]['quantity'] <= 1 ){
                        // $getsession[$key]['quantity'] = 1;
                        // $getsession[$key]['total'] = $getsession[$key]['price'] * ($getsession[$key]['quantity']);
                        unset($getsession[$key]);
                        // $check;
                        // var_dump('xóa');
                    }else{
                        $getsession[$key]['quantity'] = $getsession[$key]['quantity'] - 1; // giảm
                        $getsession[$key]['total'] = $getsession[$key]['price'] * ($getsession[$key]['quantity']);
                        // $check =false;
                    }
                }
            }
            session()->put('cart',$getsession );
            //  echo '<pre>';
        }

        // var_dump($getsession);
    }
    public function deleteToCart(Request $request){
        $getdeleteId = $request->deleteId;
        if(session()->has('cart')){
            $getdelete = session()->get('cart');
            foreach($getdelete as $key => $value){
                if($getdeleteId == $value['id']){
                    unset($getdelete[$key]);
                    session()->put('cart',$getdelete);
                }
            }

        }
        // echo '<pre>';
        // var_dump(session()->get('cart'));
    }

    public function checkout(){
        return view('frontend.cart.checkout');
    }

    // đăng ký nhanh tại đây
    public function sign_up_now(SingsnowRequest $request){
        $register = new User();
        $register->name = $request->name;
        $register->email = $request->email;
        $register->password = bcrypt($request->password);
        $register->phone = $request->phone;
        $register->address = 'default';
        $register->id_country = 1;
        $register->avatar = 'default';
        $register->level = 0;
        $register->save();
        if($register->save()){
            return redirect('/Cart/checkout/')->with("success",__("Đăng ký thành công"));
        }else{
            return redirect('/Cart/checkout/')->with("success",__("Đăng ký thất bại"));
        }
        // dd($register->save());
    }

    public function sing_up(LoginCheckoutRequest $request){
        $checkLogin = [
            'email' => $request->email,
            'password'=> $request->password,
            'level' =>0
        ];
        if(Auth::attempt($checkLogin)){
            return redirect('/Cart/checkout/')->with('success', __("Đăng nhập thành công !!!"));
        }else{
            return redirect()->back()->with('success',__("Email or Password sai"));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}