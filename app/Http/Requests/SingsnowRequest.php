<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SingsnowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>'required|max:50',
            'email' => 'required|max:50',
            'password'=>'required|max:34',
            'phone' => 'required|max:10',

        ];
    }
    public function messages()
    {
        return [
            'name.required' =>'Vui lòng nhập Tên',
            'email.required'=>'Vui lòng nhập Email',
            'password.required'=>'Vui lòng nhập Password',
            'phone.required'=>'Phone không được để trống',

        ];
    }
}
