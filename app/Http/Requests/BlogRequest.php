<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|max:255',
            'avatar'=> 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'content' =>'required',
        ];
    }
    public function messages()
    {
        return [
            'title.required' =>'Tiêu đề không được để trống',
            'avatar.required' => 'Vui lòng chọn ảnh',
            'avatar.image.max' => 'Maximum file size to upload :max',
            'avatar.mimes' => 'phải định dạng như sau:jpeg,png,jpg,gif',
            'content.required' => 'Vui lòng viết Content',
        ];
    }
}
