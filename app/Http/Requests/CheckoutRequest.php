<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' =>'required|max:50',
            'email' => 'required|max:50',
            'phone' => 'required|max:10',
            'address' => 'required|min:2|max:256',
        ];

    }
    public function messages()
    {
        return [
            'name.required' =>'Vui lòng nhập tên của bạn ?',
            'email.required'=>'Vui lòng nhập Email của bạn ?',
            'phone.required'=>'Vui lòng nhập Số điện thoại của bạn ?',
            'address.required'=>'Vui lòng nhập địa chỉ của bạn ?',
        ];
    }
}