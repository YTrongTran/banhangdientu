<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductFrontendsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|max:255',
            'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'id_category' => 'required',
            'id_brand' =>'required',
            'status' => 'required',
            'sale' => 'required',
            'company' => 'required|min:1|max:255',
            'hinhanh' => 'required',
            'hinhanh.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1024',
            'detail' => 'required|max:255',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Vui lòng nhập đầy đủ Name',
            'price.required' => 'Vui lòng nhập giá tiền',
            'id_category.required' => 'vui lòng chọn vào Category',
            'id_brand.required' => 'vui lòng chọn vào Brand',
            'status.required' => 'vui lòng chọn vào Status',
            'sale.required' => 'vui lòng nhập vào Sale:max',
            'company.required' => 'vui lòng nhập vào Campany',
            // 'hinhanh.required' => ':attribute phai la hình ảnh',
            // 'hinhanh.mimes' => ':attribute phai dinh dang như sau:jpeg,png,jpg,gif',
            // 'hinhanh.image.max' => ':attribute Maximum file size to upload :max',
            'detail.required' =>'Vui lòng nhập detail',
            'detail.required.max' =>'Vượt quá giới hạn',
        ];

    }
}
