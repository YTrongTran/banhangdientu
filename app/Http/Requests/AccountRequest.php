<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' =>'required|max:50',
            'email' => 'required|max:50',
            'password'=>'required|max:34',
            'phone' => 'required|max:10',
            'address' => 'required|max:250',
            'avatar'=> 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'id_country' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' =>'Vui lòng nhập Tên',
            'email.required'=>'Vui lòng nhập Email',
            'password.required'=>'Vui lòng nhập Password',
            'phone.required'=>'Phone không được để trống',
            'address.required'=>'Địa chỉ không được để trống',
            'avatar.required' => ':attribute phai la hình ảnh',
            'avatar.mimes' => ':attribute phai dinh dang như sau:jpeg,png,jpg,gif',
            'avatar.image.max' => ':attribute Maximum file size to upload :max',
            'id_country.required' =>'Vui lòng chọn Country',
        ];
    }
}
